<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pages extends MY_Controller {

  function __construct(){
    parent::__construct();
    $this->load->model('Pages_m');
    $this->load->library('Ajax_pagination');
    $this->perPage = 2;
  }

    public function index(){

        $data = array(
            'page_title' => 'Home',
            
        );

        $this->front('home', $data);
    }


    public function loker(){

        $data = array();
        
        $totalRec = count($this->Pages_m->getRows());
        
        //pagination configuration
        $config['target']      = '#jobList';
        $config['base_url']    = base_url().'pages/ajaxPaginationData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $config['link_func']   = 'searchFilter';
        $this->ajax_pagination->initialize($config);
        
        //get the posts data
        $data['posts'] = $this->Pages_m->getRows(array('limit'=>$this->perPage));

        $this->front('lowongan/index', $data);
    }

    public function ajaxPaginationData(){
        $conditions = array();
        
        //calc offset number
        $page = $this->input->post('page');
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }
        
        //set conditions for search
        $keywords = $this->input->post('keywords');
        $sortBy = $this->input->post('sortBy');
        if(!empty($keywords)){
            $conditions['search']['keywords'] = $keywords;
        }
        if(!empty($sortBy)){
            $conditions['search']['sortBy'] = $sortBy;
        }
        
        //total rows count
        $totalRec = count($this->Pages_m->getRows($conditions));
        
        //pagination configuration
        $config['target']      = '#jobList';
        $config['base_url']    = base_url().'posts/ajaxPaginationData';
        $config['total_rows']  = $totalRec;
        $config['per_page']    = $this->perPage;
        $config['link_func']   = 'searchFilter';
        $this->ajax_pagination->initialize($config);
        
        //set start and limit
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->perPage;
        
        //get posts data
        $data['posts'] = $this->Pages_m->getRows($conditions);
        
        //load the view
        $this->load->view('front/lowongan/data', $data, false);
    }

        
}