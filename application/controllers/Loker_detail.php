<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Loker_detail extends MY_Controller {

  function __construct(){
    parent::__construct();
    $this->load->model('Pages_m');
    $this->load->model('Loker_m');
    $this->load->library('Ajax_pagination');
    $this->perPage = 2;
  }

    public function index(){

        $job_slug = $this->uri->segment(2);

        $row = $this->Loker_m->get_by_slug($job_slug);

        $data = array(
            'page_title' => 'Home',
            'job_title' => $row->job_title,
            'job_deskripsi' => $row->job_deskripsi,
            'job_slug' => $job_slug,
            'nama_tipe' => $row->nama_tipe,
            'logo_perusahaan' => $row->logo_perusahaan,
            'nama_perusahaan' => $row->nama_perusahaan,
            'deskripsi_perusahaan' => $row->deskripsi_perusahaan,
        );

        $this->front('lowongan/detail', $data);
    }
   
}