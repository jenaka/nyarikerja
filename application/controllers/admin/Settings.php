<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Settings extends MY_Controller {

  function __construct(){
    parent::__construct();
        if ($this->session->userdata('users_login') != 1)
        redirect(base_url() . 'login', 'refresh');
    }

    public function index() {

         $data = array(
            'title' => 'Pengaturan',
            'sub' => 'Data Pengaturan'
        );
        
        $this->admin('settings', $data);
    }

}