<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Perusahaan extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Perusahaan_m');
        $this->load->library('form_validation');
    }

    public function index(){

        $data = array(
            'title' => 'Perusahaan',
            'sub' => 'Data Perusahaan',
        );
        $this->admin('perusahaan/index', $data);
    }

    public function read($id) 
    {
        $row = $this->Perusahaan_m->get_by_id($id);
        if ($row) {
            $data = array(
        		'id_perusahaan' => $row->id_perusahaan,
        		'nama_perusahaan' => $row->nama_perusahaan,
        		'email_perusahaan' => $row->email_perusahaan,
        		'telp_perusahaan' => $row->telp_perusahaan,
        		'web_perusahaan' => $row->web_perusahaan,
        		'logo_perusahaan' => $row->logo_perusahaan,
        		'id_jenis' => $row->id_jenis,
        		'deskripsi_perusahaan' => $row->deskripsi_perusahaan,
        		'log' => $row->log,
        		'lat' => $row->lat,
        		'slug_perusahaan' => $row->slug_perusahaan,
        		'status' => $row->status,
	       );
            $this->load->view('perusahaan/perusahaan_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('perusahaan'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('perusahaan/create_action'),
	    'id_perusahaan' => set_value('id_perusahaan'),
	    'nama_perusahaan' => set_value('nama_perusahaan'),
	    'email_perusahaan' => set_value('email_perusahaan'),
	    'telp_perusahaan' => set_value('telp_perusahaan'),
	    'web_perusahaan' => set_value('web_perusahaan'),
	    'logo_perusahaan' => set_value('logo_perusahaan'),
	    'id_jenis' => set_value('id_jenis'),
	    'deskripsi_perusahaan' => set_value('deskripsi_perusahaan'),
	    'log' => set_value('log'),
	    'lat' => set_value('lat'),
	    'slug_perusahaan' => set_value('slug_perusahaan'),
	    'status' => set_value('status'),
	);
        $this->load->view('perusahaan/perusahaan_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_perusahaan' => $this->input->post('nama_perusahaan',TRUE),
		'email_perusahaan' => $this->input->post('email_perusahaan',TRUE),
		'telp_perusahaan' => $this->input->post('telp_perusahaan',TRUE),
		'web_perusahaan' => $this->input->post('web_perusahaan',TRUE),
		'logo_perusahaan' => $this->input->post('logo_perusahaan',TRUE),
		'id_jenis' => $this->input->post('id_jenis',TRUE),
		'deskripsi_perusahaan' => $this->input->post('deskripsi_perusahaan',TRUE),
		'log' => $this->input->post('log',TRUE),
		'lat' => $this->input->post('lat',TRUE),
		'slug_perusahaan' => $this->input->post('slug_perusahaan',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Perusahaan_m->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('perusahaan'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Perusahaan_m->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('perusahaan/update_action'),
		'id_perusahaan' => set_value('id_perusahaan', $row->id_perusahaan),
		'nama_perusahaan' => set_value('nama_perusahaan', $row->nama_perusahaan),
		'email_perusahaan' => set_value('email_perusahaan', $row->email_perusahaan),
		'telp_perusahaan' => set_value('telp_perusahaan', $row->telp_perusahaan),
		'web_perusahaan' => set_value('web_perusahaan', $row->web_perusahaan),
		'logo_perusahaan' => set_value('logo_perusahaan', $row->logo_perusahaan),
		'id_jenis' => set_value('id_jenis', $row->id_jenis),
		'deskripsi_perusahaan' => set_value('deskripsi_perusahaan', $row->deskripsi_perusahaan),
		'log' => set_value('log', $row->log),
		'lat' => set_value('lat', $row->lat),
		'slug_perusahaan' => set_value('slug_perusahaan', $row->slug_perusahaan),
		'status' => set_value('status', $row->status),
	    );
            $this->load->view('perusahaan/perusahaan_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('perusahaan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_perusahaan', TRUE));
        } else {
            $data = array(
		'nama_perusahaan' => $this->input->post('nama_perusahaan',TRUE),
		'email_perusahaan' => $this->input->post('email_perusahaan',TRUE),
		'telp_perusahaan' => $this->input->post('telp_perusahaan',TRUE),
		'web_perusahaan' => $this->input->post('web_perusahaan',TRUE),
		'logo_perusahaan' => $this->input->post('logo_perusahaan',TRUE),
		'id_jenis' => $this->input->post('id_jenis',TRUE),
		'deskripsi_perusahaan' => $this->input->post('deskripsi_perusahaan',TRUE),
		'log' => $this->input->post('log',TRUE),
		'lat' => $this->input->post('lat',TRUE),
		'slug_perusahaan' => $this->input->post('slug_perusahaan',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Perusahaan_m->update($this->input->post('id_perusahaan', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('perusahaan'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Perusahaan_m->get_by_id($id);

        if ($row) {
            $this->Perusahaan_m->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('perusahaan'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('perusahaan'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_perusahaan', 'nama perusahaan', 'trim|required');
	$this->form_validation->set_rules('email_perusahaan', 'email perusahaan', 'trim|required');
	$this->form_validation->set_rules('telp_perusahaan', 'telp perusahaan', 'trim|required');
	$this->form_validation->set_rules('web_perusahaan', 'web perusahaan', 'trim|required');
	$this->form_validation->set_rules('logo_perusahaan', 'logo perusahaan', 'trim|required');
	$this->form_validation->set_rules('id_jenis', 'id jenis', 'trim|required');
	$this->form_validation->set_rules('deskripsi_perusahaan', 'deskripsi perusahaan', 'trim|required');
	$this->form_validation->set_rules('log', 'log', 'trim|required');
	$this->form_validation->set_rules('lat', 'lat', 'trim|required');
	$this->form_validation->set_rules('slug_perusahaan', 'slug perusahaan', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');

	$this->form_validation->set_rules('id_perusahaan', 'id_perusahaan', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Perusahaan.php */
/* Location: ./application/controllers/Perusahaan.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-11-14 07:05:58 */
/* http://harviacode.com */