<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {
    
    function __construct() {
        parent::__construct();
        
        $this->load->database();
        $this->load->library('session');
    }
    
    public function index() {
      
        if ($this->session->userdata('users_login') == 1)
        redirect(base_url() . 'admin/dashboard', 'refresh');
      
        $this->load->view('admin/login');
    }

    function validate_login() {
      $email = $this->input->post('email');
      $password = $this->input->post('password');

      $credential = array('email' => $email);

      $query = $this->Pengguna_m->check_email($email);

      if ($query->num_rows() > 0) {
          $row = $query->row();

          if($this->bcrypt->check_password($password, $row->password)){

            $this->session->set_userdata('users_login', '1');
            $this->session->set_userdata('user_id', $row->id);
            $this->session->set_userdata('nama', $row->nama);
            $this->session->set_userdata('id_akses', $row->id_akses);
            $this->session->set_userdata('hak_akses', $row->hak_akses);
            $this->session->set_flashdata('success_message', 'Login Berhasil');
          
          redirect(base_url() . 'admin/dashboard', 'refresh');
      	}else{
      		$this->session->set_flashdata('error_message', 'Login Gagal! Cek Password Anda');
    		redirect(base_url() . 'admin/login', 'refresh');
      	}

      }else{
      	$this->session->set_flashdata('error_message', 'Login Gagal! Username/Email Tidak Terdaftar');
    	redirect(base_url() . 'admin/login', 'refresh');
      }
       
    }
    
    function logout() {
        $this->session->sess_destroy();
        $this->session->set_flashdata('logout_notification', 'logged_out');
        redirect(base_url().'admin/login', 'refresh');
    }

}