<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kualifikasi extends MY_Controller {

  function __construct(){
    parent::__construct();
    $this->load->model('Kualifikasi_m');
  }

    public function index(){

        $data = array(
            'title' => 'Kualifikasi Pekerjaan',
            'sub' => 'Data Kualifikasi Pekerjaan',
            'kualifikasi_data' => $this->Kualifikasi_m->get_all(),
          ); 
          
          $this->admin('kualifikasi', $data);
    }

    public function simpan(){

        $this->_validate();

        $data = array(
            'nama_kualifikasi' => $this->input->post('nama_kualifikasi'),
            'slug' => url_title($this->input->post('nama_kualifikasi')),
        );

        $insert = $this->Kualifikasi_m->simpan($data);
        echo json_encode(array("status" => TRUE));
    }

    public function edit($id){
        $data = $this->Kualifikasi_m->get_kategori_id($id);
        echo json_encode($data);
    }


    public function update(){
        $this->_validate();

        $data = array(
            'nama_kualifikasi' => $this->input->post('nama_kualifikasi'),
            'slug' => url_title($this->input->post('nama_kualifikasi')),
        );

        $this->Kualifikasi_m->update(array('id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function hapus(){

    }

    private function _validate(){
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if($this->input->post('nama_kualifikasi') == ''){
            $data['inputerror'][] = 'nama_kualifikasi';
            $data['error_string'][] = 'Nama Industri Pekerjaan Wajib Diisi!';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
    }

    

}