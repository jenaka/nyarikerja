<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard extends MY_Controller {

  function __construct(){
    parent::__construct();
        if ($this->session->userdata('users_login') != 1)
        redirect(base_url() . 'login', 'refresh');
    }

    public function index() {

         $data = array(
            'title' => 'Beranda'
        );
        
        $this->admin('dashboard', $data);
    }

}