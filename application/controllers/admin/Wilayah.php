<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Wilayah extends MY_Controller {

  function __construct(){
    parent::__construct();
    $this->load->model('Wilayah_m');
  }

    public function index(){

        $data = array(
            'title' => 'Wilayah',
            'sub' => 'Data Provinsi',
            'provinsi_data' => $this->Wilayah_m->get_provinsi(),
          ); 
          
          $this->admin('wilayah/provinsi', $data);
    }

    public function simpan(){

        $this->_validate();

        $data = array(
            'nama_industri' => $this->input->post('nama_industri'),
            'slug' => url_title($this->input->post('nama_industri')),
            'status' => '1',
        );

        $insert = $this->Industri_m->simpan($data);
        echo json_encode(array("status" => TRUE));
    }

    public function edit($id){
        $data = $this->Industri_m->get_kategori_id($id);
        echo json_encode($data);
    }


    public function update(){
        $this->_validate();

        $data = array(
            'nama_industri' => $this->input->post('nama_industri'),
            'slug' => url_title($this->input->post('nama_industri')),
            'status' => '1',
        );

        $this->Industri_m->update(array('id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function hapus(){

    }

    private function _validate(){
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if($this->input->post('nama_industri') == ''){
            $data['inputerror'][] = 'nama_industri';
            $data['error_string'][] = 'Nama Industri Pekerjaan Wajib Diisi!';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
    }


    public function kota($id){

        $row = $this->Wilayah_m->get_provinsi_id($id);


        $data = array(
            'title' => 'Wilayah',
            'sub' => 'Data Kota/Kabupaten Provinsi '.$row->name,
            'kota_data' => $this->Wilayah_m->get_kota($id),
          ); 
          
          $this->admin('wilayah/kota', $data);
    }




    public function kecamatan($id){

        $row = $this->Wilayah_m->get_kota_id($id);


        $data = array(
            'title' => 'Wilayah',
            'sub' => 'Data Kecamatan '.$row->name,
            'kecamatan_data' => $this->Wilayah_m->get_kecamatan($id),
          ); 
          
          $this->admin('wilayah/kecamatan', $data);
    }



    public function kelurahan($id){

        $row = $this->Wilayah_m->get_kecamatan_id($id);


        $data = array(
            'title' => 'Wilayah',
            'sub' => 'Data Desa/Kelurahan Kecamatan '.$row->name,
            'kelurahan_data' => $this->Wilayah_m->get_kelurahan($id),
          ); 
          
          $this->admin('wilayah/kelurahan', $data);
    }
    

}