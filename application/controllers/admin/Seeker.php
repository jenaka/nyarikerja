<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Seeker extends MY_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Seeker_m');
        $this->load->library('form_validation');
    }

    public function index(){

        $data = array(
        	'title' => 'Job Seeker',
        	'sub' => 'Data Job Seeker',
        );
        $this->admin('seeker/index', $data);
    }

    public function detail($id){
        $row = $this->Seeker_m->get_by_id($id);
        if ($row) {
            $data = array(
				'id_seeker' => $row->id_seeker,
				'nama' => $row->nama,
				'username' => $row->username,
				'email' => $row->email,
				'password' => $row->password,
				'tmp_lahir' => $row->tmp_lahir,
				'tgl_lahir' => $row->tgl_lahir,
				'jk' => $row->jk,
				'no_hp' => $row->no_hp,
				'agama' => $row->agama,
				'kewarganegaraan' => $row->kewarganegaraan,
				'status' => $row->status,
				'pend_terkahir' => $row->pend_terkahir,
				'id_kecamatan' => $row->id_kecamatan,
				'id_kelurahan' => $row->id_kelurahan,
				'alamat' => $row->alamat,
	    	);
	    	
            $this->load->view('seeker/seeker_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('seeker'));
        }
    }

    public function tambah(){
        $data = array(
            'button' => 'Simpan',
            'title' => 'Job Seeker',
            'sub' => 'Tambah Job Seeker',
            'action' => site_url('seeker/simpan'),
		    'id_seeker' => set_value('id_seeker'),
		    'nama' => set_value('nama'),
		    'username' => set_value('username'),
		    'email' => set_value('email'),
		    'password' => set_value('password'),
		    'tmp_lahir' => set_value('tmp_lahir'),
		    'tgl_lahir' => set_value('tgl_lahir'),
		    'jk' => set_value('jk'),
		    'no_hp' => set_value('no_hp'),
		    'agama' => set_value('agama'),
		    'kewarganegaraan' => set_value('kewarganegaraan'),
		    'status' => set_value('status'),
		    'pend_terkahir' => set_value('pend_terkahir'),
		    'id_kecamatan' => set_value('id_kecamatan'),
		    'id_kelurahan' => set_value('id_kelurahan'),
		    'alamat' => set_value('alamat'),
		);

        $this->admin('seeker/form', $data);
    }
    
    public function create_action(){
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama' => $this->input->post('nama',TRUE),
		'username' => $this->input->post('username',TRUE),
		'email' => $this->input->post('email',TRUE),
		'password' => $this->input->post('password',TRUE),
		'tmp_lahir' => $this->input->post('tmp_lahir',TRUE),
		'tgl_lahir' => $this->input->post('tgl_lahir',TRUE),
		'jk' => $this->input->post('jk',TRUE),
		'no_hp' => $this->input->post('no_hp',TRUE),
		'agama' => $this->input->post('agama',TRUE),
		'kewarganegaraan' => $this->input->post('kewarganegaraan',TRUE),
		'status' => $this->input->post('status',TRUE),
		'pend_terkahir' => $this->input->post('pend_terkahir',TRUE),
		'id_kecamatan' => $this->input->post('id_kecamatan',TRUE),
		'id_kelurahan' => $this->input->post('id_kelurahan',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
	    );

            $this->Seeker_m->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('seeker'));
        }
    }
    
    public function update($id){
        $row = $this->Seeker_m->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('seeker/update_action'),
		'id_seeker' => set_value('id_seeker', $row->id_seeker),
		'nama' => set_value('nama', $row->nama),
		'username' => set_value('username', $row->username),
		'email' => set_value('email', $row->email),
		'password' => set_value('password', $row->password),
		'tmp_lahir' => set_value('tmp_lahir', $row->tmp_lahir),
		'tgl_lahir' => set_value('tgl_lahir', $row->tgl_lahir),
		'jk' => set_value('jk', $row->jk),
		'no_hp' => set_value('no_hp', $row->no_hp),
		'agama' => set_value('agama', $row->agama),
		'kewarganegaraan' => set_value('kewarganegaraan', $row->kewarganegaraan),
		'status' => set_value('status', $row->status),
		'pend_terkahir' => set_value('pend_terkahir', $row->pend_terkahir),
		'id_kecamatan' => set_value('id_kecamatan', $row->id_kecamatan),
		'id_kelurahan' => set_value('id_kelurahan', $row->id_kelurahan),
		'alamat' => set_value('alamat', $row->alamat),
	    );
            $this->load->view('seeker/seeker_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('seeker'));
        }
    }
    
    public function update_action(){
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_seeker', TRUE));
        } else {
            $data = array(
		'nama' => $this->input->post('nama',TRUE),
		'username' => $this->input->post('username',TRUE),
		'email' => $this->input->post('email',TRUE),
		'password' => $this->input->post('password',TRUE),
		'tmp_lahir' => $this->input->post('tmp_lahir',TRUE),
		'tgl_lahir' => $this->input->post('tgl_lahir',TRUE),
		'jk' => $this->input->post('jk',TRUE),
		'no_hp' => $this->input->post('no_hp',TRUE),
		'agama' => $this->input->post('agama',TRUE),
		'kewarganegaraan' => $this->input->post('kewarganegaraan',TRUE),
		'status' => $this->input->post('status',TRUE),
		'pend_terkahir' => $this->input->post('pend_terkahir',TRUE),
		'id_kecamatan' => $this->input->post('id_kecamatan',TRUE),
		'id_kelurahan' => $this->input->post('id_kelurahan',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
	    );

            $this->Seeker_m->update($this->input->post('id_seeker', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('seeker'));
        }
    }
    
    public function delete($id){
        $row = $this->Seeker_m->get_by_id($id);

        if ($row) {
            $this->Seeker_m->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('seeker'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('seeker'));
        }
    }

    public function _rules(){
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
	$this->form_validation->set_rules('username', 'username', 'trim|required');
	$this->form_validation->set_rules('email', 'email', 'trim|required');
	$this->form_validation->set_rules('password', 'password', 'trim|required');
	$this->form_validation->set_rules('tmp_lahir', 'tmp lahir', 'trim|required');
	$this->form_validation->set_rules('tgl_lahir', 'tgl lahir', 'trim|required');
	$this->form_validation->set_rules('jk', 'jk', 'trim|required');
	$this->form_validation->set_rules('no_hp', 'no hp', 'trim|required');
	$this->form_validation->set_rules('agama', 'agama', 'trim|required');
	$this->form_validation->set_rules('kewarganegaraan', 'kewarganegaraan', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');
	$this->form_validation->set_rules('pend_terkahir', 'pend terkahir', 'trim|required');
	$this->form_validation->set_rules('id_kecamatan', 'id kecamatan', 'trim|required');
	$this->form_validation->set_rules('id_kelurahan', 'id kelurahan', 'trim|required');
	$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');

	$this->form_validation->set_rules('id_seeker', 'id_seeker', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}
