<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Pengguna extends MY_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Pengguna_m');
    if ($this->session->userdata('users_login') != 1)
            redirect(base_url() . 'login', 'refresh');
  }

  public function index(){

    $data = array(
      'title' => 'Pengguna',
      'sub' => 'Data Pengguna',
      'users_data' => $this->Pengguna_m->get_all_pengguna(),
    );


    $this->admin('pengguna/index', $data);
  }

  public function tambah(){

    $data = array(
      'title' => 'Pengguna',
      'sub' => 'Tambah Pengguna',
      'action' => site_url('admin/pengguna/simpan'),
      'id' => set_value('id'),
      'nama' => set_value('nama'),
      'username' => set_value('username'),
      'email' => set_value('email'),
      'password' => set_value('password'),
      'password_knf' => set_value('password_knf'),
      'jabatan_data' => $this->Pengguna_m->get_akses_all(),
      'id_akses' => set_value('id_akses'),
    );

    $this->admin('pengguna/tambah', $data);
  }

  public function simpan(){

    $this->form_validation->set_rules('nama', 'Nama Lengkap', 'trim|required');
    $this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[users.username]');
    $this->form_validation->set_rules('email', 'Alamat Email', 'trim|valid_email|required|is_unique[users.email]');
    $this->form_validation->set_rules('password', 'Password', 'trim|required');
    $this->form_validation->set_rules('id_akses', 'Jabatan Pengguna', 'trim|required');
    $this->form_validation->set_rules('password_knf', 'Konfirmasi Password', 'required|matches[password]');
    if ($this->form_validation->run() == FALSE) {
            $this->tambah();
    } else {
        $data = array(
          'nama' => $this->input->post('nama',TRUE),
          'email' => $this->input->post('email',TRUE),
          'username' => $this->input->post('username',TRUE),
          'password' => $this->bcrypt->hash_password($this->input->post('password',TRUE)),
          'id_akses' => $this->input->post('id_akses',TRUE),
          'status' => '1',
        );

        $this->Pengguna_m->tambah($data);
        $this->session->set_flashdata('success_message', 'Data Pengguna Berhasil Ditambahkan');
        redirect(site_url('admin/pengguna'));
    }
  }

  public function detail($id){

    $row = $this->Pengguna_m->get_by_id($id);

    if ($row) {
        $data = array(
            'title' => 'Pengguna',
            'sub' => 'Detail Pengguna',
            'action' => '',
            'id' => set_value('id', $row->id),
            'nama' => set_value('nama', $row->nama),
            'username' => set_value('username', $row->username),
            'email' => set_value('email', $row->email),
            'id_akses' => set_value('id_akses', $row->id_akses),
            'jabatan_data' => $this->Pengguna_m->get_akses_all(),
        );
          $this->admin('pengguna/detail', $data);
      } else {
          $this->session->set_flashdata('error_message', 'Data Pengguna Tidak Ditemukan');
          redirect(site_url('admin/pengguna'));
      }
    }

  public function edit($id){

    $row = $this->Pengguna_m->get_by_id($id);

    if ($row) {
        $data = array(
            'title' => 'Pengguna',
            'sub' => 'Edit Pengguna',
            'action' => site_url('admin/pengguna/update'),
            'id' => set_value('id', $row->id),
            'nama' => set_value('nama', $row->nama),
            'username' => set_value('username', $row->username),
            'email' => set_value('email', $row->email),
            'id_akses' => set_value('id_akses', $row->id_akses),
            'jabatan_data' => $this->Pengguna_m->get_akses_all(),
        );

          $this->admin('pengguna/edit', $data);
      } else {
          $this->session->set_flashdata('error_message', 'Data Pengguna Tidak Ditemukan');
          redirect(site_url('admin/pengguna'));
      }
    }

   public function update(){

      $this->form_validation->set_rules('NIP', 'nip', 'trim|required');
      $this->form_validation->set_rules('nama', 'nama', 'trim|required');
      $this->form_validation->set_rules('username', 'Username', 'trim|required');
      $this->form_validation->set_rules('email', 'Alamat Email', 'trim|required');
      $this->form_validation->set_rules('telp', 'telp', 'trim|required');

      if ($this->form_validation->run() == FALSE) {
          $this->jenis_surat();
      } else {
          $data = array(
            'NIP' => $this->input->post('NIP',TRUE),
            'nama' => $this->input->post('nama',TRUE),
            'email' => $this->input->post('email',TRUE),
            'username' => $this->input->post('username'),
            'telp' => $this->input->post('telp',TRUE),
            'id_akses' => $this->input->post('id_akses',TRUE),
          );

          $this->Pengguna_m->update(array('id' => $this->input->post('id')), $data);
          $this->session->set_flashdata('success_message', 'Data Pengguna Berhasil Diperbaharui');
          redirect(site_url('admin/pengguna'));
      }
   }

   public function trash(){
        
        $data = array(
            'title' => 'Pengguna',
            'sub' => 'Data Pengguna Dihapus',
            'owner_data' => $this->Owner_m->get_trash(),
        );

        $this->admin('owner/trash', $data);
    }

    public function aktif($id){
        $data = array('active' => 1);
        $this->Pengguna_m->aktivasi($id, $data);
        echo json_encode(array("status" => TRUE));
    }

    public function nonaktif($id){
        $data = array('active' => 0);
        $this->Pengguna_m->aktivasi($id, $data);
        echo json_encode(array("status" => TRUE));
    }

    public function hapus($id){
        $data = array('dihapus' => 'Y');
        $this->Pengguna_m->hapus($id, $data);
        echo json_encode(array("status" => TRUE));
    }

    public function restore($id){
        $data = array('dihapus' => 'T');
        $this->Pengguna_m->hapus($id, $data);
        echo json_encode(array("status" => TRUE));
    }

    public function permanen($id){
        $this->Pengguna_m->permanen($id);
        echo json_encode(array("status" => TRUE));
    }


    public function update_pw(){
        
        $this->form_validation->set_rules('password_lama', 'Password Lama', 'trim|required');
        $this->form_validation->set_rules('password_baru', 'Password Baru', 'trim|required');
        $this->form_validation->set_rules('password_baru_knf', 'Konfirmasi Password Baru', 'required|matches[password_baru]');
        if ($this->form_validation->run() == FALSE) {
            $this->ganti_password();
        } else {

            $id = $this->session->userdata('login_user_id');
            $password = $this->input->post('password_lama',TRUE);

            if($this->session->userdata('login_type') == 'Kasi'){
                $get_id = $this->Operator_m->get_by_id($id);
            }else{
                $get_id = $this->Pengguna_m->get_by_id($id);
            }

            $row = $get_id;

            if($this->bcrypt->check_password($password, $row->password)){

                $data = array(
                    'password' => $this->bcrypt->hash_password($this->input->post('password_baru',TRUE))
                );

                if($this->session->userdata('login_type') == 'Kasi'){
                    $this->Kasi_m->update(array('id' => $id), $data);
                }else{
                    $this->Pengguna_m->update(array('id' => $id), $data);
                }
                $this->session->set_flashdata('success_message', 'Password Berhasil Diperbaharui');
                redirect(site_url('beranda'));
                
            }else{
                $this->session->set_flashdata('error_message', 'Password Lama Salah!');
                redirect(base_url() . 'pengguna/ganti_password', 'refresh');
            }   
        }

    }

  //Akses
  public function akses(){
      $data = array(
        'title' => 'Jabatan Pengguna',
        'sub' => 'Data Jabatan Pengguna',
        'akses_data' => $this->Pengguna_m->get_akses_all(),
        'nama' => set_value('nama'),
      );
    $this->admin('pengguna/akses', $data);
  }

    public function tambah_akses(){
      $this->form_validation->set_rules('nama', 'Nama Jabatan', 'trim|required');
      if ($this->form_validation->run() == FALSE) {
          $this->jenis_surat();
      } else {
          $data = array(
                  'nama' => ucwords($this->input->post('nama')),
              );

          $this->Pengguna_m->simpan_akses($data);
          $this->session->set_flashdata('success_message', 'Akses Surat Baru Berhasil Ditambahkan');
          redirect(site_url('pengguna/akses'));
      }

    }

  public function akses_edit($id){

            $data = $this->Pengguna_m->get_akses_by_id($id);

            echo json_encode($data);
    }

   public function update_akses(){

      $this->form_validation->set_rules('nama', 'Nama Akses Surat', 'trim|required');

      if ($this->form_validation->run() == FALSE) {
          $this->jenis_surat();
      } else {
          $data = array(
                  'nama' => ucwords($this->input->post('nama')),
              );

          $this->Pengguna_m->update_akses(array('id' => $this->input->post('id')), $data);
          $this->session->set_flashdata('success_message', 'Akses Surat Berhasil Diperbaharui');
          redirect(site_url('pengguna/akses'));
      }
   }

    public function hapus_akses($id){
        $this->Pengguna_m->hapus_akses($id);
        echo json_encode(array("status" => TRUE));
    }

    public function ganti_password(){



      $this->admin('pengguna/ganti_pw.php');
    }
}