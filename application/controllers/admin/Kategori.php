<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kategori extends MY_Controller {

  function __construct(){
    parent::__construct();
    $this->load->model('Kategori_m');
  }

    public function index(){

        $data = array(
            'title' => 'Kategori Lowongan Kerja',
            'sub' => 'Data Kategori Lowongan Kerja',
            'kategori_data' => $this->Kategori_m->get_all(),
          ); 
          
          $this->admin('loker/kategori', $data);
    }

    public function simpan(){

        $this->_validate();

        $data = array(
            'nama' => $this->input->post('nama_kategori'),
            'slug' => url_title($this->input->post('nama_kategori')),
        );

        $insert = $this->Kategori_m->simpan($data);
        echo json_encode(array("status" => TRUE));
    }

    public function edit($id){
        $data = $this->Kategori_m->get_kategori_id($id);
        echo json_encode($data);
    }


    public function update(){
        $this->_validate();

        $data = array(
            'nama_kategori' => $this->input->post('nama_kategori'),
            'slug' => url_title($this->input->post('nama_kategori')),
            'status' => '1',
        );

        $this->Kategori_m->update(array('id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function hapus(){

    }

    private function _validate(){
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if($this->input->post('nama_kategori') == ''){
            $data['inputerror'][] = 'nama_kategori';
            $data['error_string'][] = 'Nama Kategori Pekerjaan Wajib Diisi!';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
    }


    public function sub($id){

        $row = $this->Kategori_m->get_kategori_id($id);

        $data = array(
            'title' => 'Kategori Lowongan Kerja',
            'sub' => 'Data Sub Kategori '.$row->nama,
            'id_kategori' => $row->id_kategori,
            'kategori_data' => $this->Kategori_m->get_sub_all($id),
            'nama_kategori' => $row->nama,
          ); 
          
          $this->admin('loker/sub_kategori', $data);   
    }

    public function sub_simpan(){

        $this->_validate();

        $data = array(
            'nama' => $this->input->post('nama_kategori'),
            'slug' => url_title($this->input->post('nama_kategori')),
            'id_kategori' => $this->input->post('id_kategori'),
        );

        $insert = $this->Kategori_m->sub_simpan($data);
        echo json_encode(array("status" => TRUE));
    }

    public function sub_edit($id){
        $data = $this->Kategori_m->get_kategori_id($id);
        echo json_encode($data);
    }


    public function sub_update(){
        $this->_validate();

        $data = array(
            'nama' => $this->input->post('nama_kategori'),
            'slug' => url_title($this->input->post('nama_kategori')),
            'id_kategori' => $this->input->post('id_kategori'),
        );

        $this->Kategori_m->update(array('id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function sub_hapus(){

    }    

}