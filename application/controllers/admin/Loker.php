<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Loker extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Loker_m');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'loker/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'loker/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'loker/index.html';
            $config['first_url'] = base_url() . 'loker/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Loker_m->total_rows($q);
        $loker = $this->Loker_m->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'loker_data' => $loker,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->admin('loker/index', $data);
    }

    public function read($id){
        $row = $this->Loker_m->get_by_id($id);
        if ($row) {
            $data = array(
				'id_job' => $row->id_job,
				'job_title' => $row->job_title,
				'job_slug' => $row->job_slug,
				'id_pegawai' => $row->id_pegawai,
				'id_perusahaan' => $row->id_perusahaan,
				'id_industri' => $row->id_industri,
				'gaji' => $row->gaji,
				'tgl' => $row->tgl,
				'status' => $row->status,
				'is_featured' => $row->is_featured,
				'country' => $row->country,
				'age_required' => $row->age_required,
				'id_kualifikasi' => $row->id_kualifikasi,
				'pengalaman' => $row->pengalaman,
				'city' => $row->city,
				'last_date' => $row->last_date,
				'id_job_tipe' => $row->id_job_tipe,
				'vacancies' => $row->vacancies,
				'job_deskripsi' => $row->job_deskripsi,
				'contact_person' => $row->contact_person,
				'contact_email' => $row->contact_email,
				'contact_phone' => $row->contact_phone,
				'viewer_count' => $row->viewer_count,
				'ip_address' => $row->ip_address,
	    	);
            $this->admin('loker/job_post_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('loker'));
        }
    }

    public function tambah(){
        $data = array(
            'button' => 'Create',
            'action' => site_url('loker/create_action'),
		    'id_job' => set_value('id_job'),
		    'job_title' => set_value('job_title'),
		    'job_slug' => set_value('job_slug'),
		    'id_pegawai' => set_value('id_pegawai'),
		    'id_perusahaan' => set_value('id_perusahaan'),
		    'id_industri' => set_value('id_industri'),
		    'gaji' => set_value('gaji'),
		    'tgl' => set_value('tgl'),
		    'status' => set_value('status'),
		    'is_featured' => set_value('is_featured'),
		    'country' => set_value('country'),
		    'age_required' => set_value('age_required'),
		    'id_kualifikasi' => set_value('id_kualifikasi'),
		    'pengalaman' => set_value('pengalaman'),
		    'city' => set_value('city'),
		    'last_date' => set_value('last_date'),
		    'id_job_tipe' => set_value('id_job_tipe'),
		    'vacancies' => set_value('vacancies'),
		    'job_deskripsi' => set_value('job_deskripsi'),
		    'contact_person' => set_value('contact_person'),
		    'contact_email' => set_value('contact_email'),
		    'contact_phone' => set_value('contact_phone'),
		    'viewer_count' => set_value('viewer_count'),
		    'ip_address' => set_value('ip_address'),
		);
        $this->admin('loker/form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'job_title' => $this->input->post('job_title',TRUE),
		'job_slug' => $this->input->post('job_slug',TRUE),
		'id_pegawai' => $this->input->post('id_pegawai',TRUE),
		'id_perusahaan' => $this->input->post('id_perusahaan',TRUE),
		'id_industri' => $this->input->post('id_industri',TRUE),
		'gaji' => $this->input->post('gaji',TRUE),
		'tgl' => $this->input->post('tgl',TRUE),
		'status' => $this->input->post('status',TRUE),
		'is_featured' => $this->input->post('is_featured',TRUE),
		'country' => $this->input->post('country',TRUE),
		'age_required' => $this->input->post('age_required',TRUE),
		'id_kualifikasi' => $this->input->post('id_kualifikasi',TRUE),
		'pengalaman' => $this->input->post('pengalaman',TRUE),
		'city' => $this->input->post('city',TRUE),
		'last_date' => $this->input->post('last_date',TRUE),
		'id_job_tipe' => $this->input->post('id_job_tipe',TRUE),
		'vacancies' => $this->input->post('vacancies',TRUE),
		'job_deskripsi' => $this->input->post('job_deskripsi',TRUE),
		'contact_person' => $this->input->post('contact_person',TRUE),
		'contact_email' => $this->input->post('contact_email',TRUE),
		'contact_phone' => $this->input->post('contact_phone',TRUE),
		'viewer_count' => $this->input->post('viewer_count',TRUE),
		'ip_address' => $this->input->post('ip_address',TRUE),
	    );

            $this->Loker_m->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('loker'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Loker_m->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('loker/update_action'),
		'id_job' => set_value('id_job', $row->id_job),
		'job_title' => set_value('job_title', $row->job_title),
		'job_slug' => set_value('job_slug', $row->job_slug),
		'id_pegawai' => set_value('id_pegawai', $row->id_pegawai),
		'id_perusahaan' => set_value('id_perusahaan', $row->id_perusahaan),
		'id_industri' => set_value('id_industri', $row->id_industri),
		'gaji' => set_value('gaji', $row->gaji),
		'tgl' => set_value('tgl', $row->tgl),
		'status' => set_value('status', $row->status),
		'is_featured' => set_value('is_featured', $row->is_featured),
		'country' => set_value('country', $row->country),
		'age_required' => set_value('age_required', $row->age_required),
		'id_kualifikasi' => set_value('id_kualifikasi', $row->id_kualifikasi),
		'pengalaman' => set_value('pengalaman', $row->pengalaman),
		'city' => set_value('city', $row->city),
		'last_date' => set_value('last_date', $row->last_date),
		'id_job_tipe' => set_value('id_job_tipe', $row->id_job_tipe),
		'vacancies' => set_value('vacancies', $row->vacancies),
		'job_deskripsi' => set_value('job_deskripsi', $row->job_deskripsi),
		'contact_person' => set_value('contact_person', $row->contact_person),
		'contact_email' => set_value('contact_email', $row->contact_email),
		'contact_phone' => set_value('contact_phone', $row->contact_phone),
		'viewer_count' => set_value('viewer_count', $row->viewer_count),
		'ip_address' => set_value('ip_address', $row->ip_address),
	    );
            $this->load->view('loker/job_post_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('loker'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_job', TRUE));
        } else {
            $data = array(
		'job_title' => $this->input->post('job_title',TRUE),
		'job_slug' => $this->input->post('job_slug',TRUE),
		'id_pegawai' => $this->input->post('id_pegawai',TRUE),
		'id_perusahaan' => $this->input->post('id_perusahaan',TRUE),
		'id_industri' => $this->input->post('id_industri',TRUE),
		'gaji' => $this->input->post('gaji',TRUE),
		'tgl' => $this->input->post('tgl',TRUE),
		'status' => $this->input->post('status',TRUE),
		'is_featured' => $this->input->post('is_featured',TRUE),
		'country' => $this->input->post('country',TRUE),
		'age_required' => $this->input->post('age_required',TRUE),
		'id_kualifikasi' => $this->input->post('id_kualifikasi',TRUE),
		'pengalaman' => $this->input->post('pengalaman',TRUE),
		'city' => $this->input->post('city',TRUE),
		'last_date' => $this->input->post('last_date',TRUE),
		'id_job_tipe' => $this->input->post('id_job_tipe',TRUE),
		'vacancies' => $this->input->post('vacancies',TRUE),
		'job_deskripsi' => $this->input->post('job_deskripsi',TRUE),
		'contact_person' => $this->input->post('contact_person',TRUE),
		'contact_email' => $this->input->post('contact_email',TRUE),
		'contact_phone' => $this->input->post('contact_phone',TRUE),
		'viewer_count' => $this->input->post('viewer_count',TRUE),
		'ip_address' => $this->input->post('ip_address',TRUE),
	    );

            $this->Loker_m->update($this->input->post('id_job', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('loker'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Loker_m->get_by_id($id);

        if ($row) {
            $this->Loker_m->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('loker'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('loker'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('job_title', 'job title', 'trim|required');
	$this->form_validation->set_rules('job_slug', 'job slug', 'trim|required');
	$this->form_validation->set_rules('id_pegawai', 'id pegawai', 'trim|required');
	$this->form_validation->set_rules('id_perusahaan', 'id perusahaan', 'trim|required');
	$this->form_validation->set_rules('id_industri', 'id industri', 'trim|required');
	$this->form_validation->set_rules('gaji', 'gaji', 'trim|required');
	$this->form_validation->set_rules('tgl', 'tgl', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');
	$this->form_validation->set_rules('is_featured', 'is featured', 'trim|required');
	$this->form_validation->set_rules('country', 'country', 'trim|required');
	$this->form_validation->set_rules('age_required', 'age required', 'trim|required');
	$this->form_validation->set_rules('id_kualifikasi', 'id kualifikasi', 'trim|required');
	$this->form_validation->set_rules('pengalaman', 'pengalaman', 'trim|required');
	$this->form_validation->set_rules('city', 'city', 'trim|required');
	$this->form_validation->set_rules('last_date', 'last date', 'trim|required');
	$this->form_validation->set_rules('id_job_tipe', 'id job tipe', 'trim|required');
	$this->form_validation->set_rules('vacancies', 'vacancies', 'trim|required');
	$this->form_validation->set_rules('job_deskripsi', 'job deskripsi', 'trim|required');
	$this->form_validation->set_rules('contact_person', 'contact person', 'trim|required');
	$this->form_validation->set_rules('contact_email', 'contact email', 'trim|required');
	$this->form_validation->set_rules('contact_phone', 'contact phone', 'trim|required');
	$this->form_validation->set_rules('viewer_count', 'viewer count', 'trim|required');
	$this->form_validation->set_rules('ip_address', 'ip address', 'trim|required');

	$this->form_validation->set_rules('id_job', 'id_job', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Loker.php */
/* Location: ./application/controllers/Loker.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-11-24 06:26:00 */
/* http://harviacode.com */