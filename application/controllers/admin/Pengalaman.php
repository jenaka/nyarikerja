<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pengalaman extends MY_Controller {

  function __construct(){
    parent::__construct();
    $this->load->model('Pengalaman_m');
  }

    public function index(){

        $data = array(
            'title' => 'Pengalaman Pekerjaan',
            'sub' => 'Data Pengalaman Pekerjaan',
            'kategori_data' => $this->Pengalaman_m->get_all(),
          ); 
          
          $this->admin('pengalaman', $data);
    }

    public function simpan(){

        $this->_validate();

        $data = array(
            'nama' => $this->input->post('nama'),
        );

        $insert = $this->Pengalaman_m->simpan($data);
        echo json_encode(array("status" => TRUE));
    }

    public function edit($id){
        $data = $this->Pengalaman_m->get_pengalaman_id($id);
        echo json_encode($data);
    }


    public function update(){
        $this->_validate();

        $data = array(
            'nama' => $this->input->post('nama'),
        );

        $this->Pengalaman_m->update(array('id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function hapus(){

    }

    private function _validate(){
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if($this->input->post('nama') == ''){
            $data['inputerror'][] = 'nama';
            $data['error_string'][] = 'Nama Pengalaman Pekerjaan Wajib Diisi!';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
    }

    

}