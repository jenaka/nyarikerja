<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Tipe extends MY_Controller {

  function __construct(){
    parent::__construct();
    $this->load->model('Tipe_m');
  }

    public function index(){

        $data = array(
            'title' => 'Tipe Pekerjaan',
            'sub' => 'Data Tipe Pekerjaan',
            'kategori_data' => $this->Tipe_m->get_all(),
          ); 
          
          $this->admin('job_tipe', $data);
    }

    public function simpan(){

        $this->_validate();

        $data = array(
            'nama_tipe' => $this->input->post('nama_tipe'),
            'slug' => url_title($this->input->post('nama_tipe')),
        );

        $insert = $this->Tipe_m->simpan($data);
        echo json_encode(array("status" => TRUE));
    }

    public function edit($id){
        $data = $this->Tipe_m->get_tipe_id($id);
        echo json_encode($data);
    }


    public function update(){
        $this->_validate();

        $data = array(
            'nama_tipe' => $this->input->post('nama_tipe'),
            'slug' => url_title($this->input->post('nama_tipe')),
        );

        $this->Tipe_m->update(array('id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function hapus(){

    }

    private function _validate(){
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if($this->input->post('nama_tipe') == ''){
            $data['inputerror'][] = 'nama_tipe';
            $data['error_string'][] = 'Nama Tipe Pekerjaan Wajib Diisi!';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
    }

    

}