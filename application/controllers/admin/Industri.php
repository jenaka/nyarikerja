<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Industri extends MY_Controller {

  function __construct(){
    parent::__construct();
    $this->load->model('Industri_m');
  }

    public function index(){

        $data = array(
            'title' => 'Industri Pekerjaan',
            'sub' => 'Data Industri Pekerjaan',
            'kategori_data' => $this->Industri_m->get_all(),
          ); 
          
          $this->admin('industri', $data);
    }

    public function simpan(){

        $this->_validate();

        $data = array(
            'nama_industri' => $this->input->post('nama_industri'),
            'slug' => url_title($this->input->post('nama_industri')),
            'status' => '1',
        );

        $insert = $this->Industri_m->simpan($data);
        echo json_encode(array("status" => TRUE));
    }

    public function edit($id){
        $data = $this->Industri_m->get_kategori_id($id);
        echo json_encode($data);
    }


    public function update(){
        $this->_validate();

        $data = array(
            'nama_industri' => $this->input->post('nama_industri'),
            'slug' => url_title($this->input->post('nama_industri')),
            'status' => '1',
        );

        $this->Industri_m->update(array('id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function hapus(){

    }

    private function _validate(){
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if($this->input->post('nama_industri') == ''){
            $data['inputerror'][] = 'nama_industri';
            $data['error_string'][] = 'Nama Industri Pekerjaan Wajib Diisi!';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
    }

    

}