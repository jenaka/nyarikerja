<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Gaji extends MY_Controller {

  function __construct(){
    parent::__construct();
    $this->load->model('Gaji_m');
  }

    public function index(){

        $data = array(
            'title' => 'Gaji Pekerjaan',
            'sub' => 'Data Gaji Pekerjaan',
            'kategori_data' => $this->Gaji_m->get_all(),
          ); 
          
          $this->admin('gaji', $data);
    }

    public function simpan(){

        $this->_validate();

        $data = array(
            'judul' => $this->input->post('judul'),
            'jumlah' => $this->input->post('jumlah'),
            'slug' => url_title($this->input->post('jumlah')),
        );

        $insert = $this->Gaji_m->simpan($data);
        echo json_encode(array("status" => TRUE));
    }

    public function edit($id){
        $data = $this->Gaji_m->get_gaji_id($id);
        echo json_encode($data);
    }


    public function update(){
        $this->_validate();

        $data = array(
            'nama_gaji' => $this->input->post('nama_gaji'),
            'slug' => url_title($this->input->post('nama_gaji')),
            'status' => '1',
        );

        $this->Gaji_m->update(array('id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function hapus(){

    }

    private function _validate(){
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if($this->input->post('jumlah') == ''){
            $data['inputerror'][] = 'jumlah';
            $data['error_string'][] = 'Jumlah Gaji Pekerjaan Wajib Diisi!';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
    }

    

}