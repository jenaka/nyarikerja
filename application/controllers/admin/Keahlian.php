<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Keahlian extends MY_Controller {

  function __construct(){
    parent::__construct();
    $this->load->model('Keahlian_m');
  }

    public function index(){

        $data = array(
            'title' => 'Keahlian Pekerjaan',
            'sub' => 'Data Keahlian Pekerjaan',
            'keahlian_data' => $this->Keahlian_m->get_all(),
          ); 
          
          $this->admin('keahlian', $data);
    }

    public function simpan(){

        $this->_validate();

        $data = array(
            'nama_skill' => $this->input->post('nama_skill'),
        );

        $insert = $this->Keahlian_m->simpan($data);
        echo json_encode(array("status" => TRUE));
    }

    public function edit($id){
        $data = $this->Keahlian_m->get_keahlian_id($id);
        echo json_encode($data);
    }


    public function update(){
        $this->_validate();

        $data = array(
            'nama_skill' => $this->input->post('nama_skill'),
        );

        $this->Keahlian_m->update(array('id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function hapus(){

    }

    private function _validate(){
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if($this->input->post('nama_skill') == ''){
            $data['inputerror'][] = 'nama_skill';
            $data['error_string'][] = 'Nama Keahlian Pekerjaan Wajib Diisi!';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
    }

    

}