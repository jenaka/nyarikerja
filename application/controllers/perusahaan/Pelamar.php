<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pelamar extends MY_Controller {

  function __construct(){
    parent::__construct();

        if ($this->session->userdata('perusahaan_login') != 1)
        redirect(base_url() . 'login', 'refresh');
    }

    public function index() {

        $row = $this->Perusahaan_m->get_profile_id($this->session->userdata('id_perusahaan'));

         $data = array(
            'title' => 'Beranda',
            'nama_perusahaan' => $row->nama_perusahaan,
            'id_jenis' => $row->id_jenis,
            'deskripsi_perusahaan' => $row->deskripsi_perusahaan,
            'id_provinsi' => $row->id_provinsi,
            'id_kota' => $row->id_kota,
            'alamat_perusahaan' => $row->alamat_perusahaan,
            'logo_perusahaan' => $row->logo_perusahaan,
            'web_perusahaan' => $row->web_perusahaan,
            'industri_data' => $this->Industri_m->get_all(),
            'provinsi' => $this->Wilayah_m->get_provinsi(),
            'kota' => $this->Wilayah_m->get_kota_prov(),
            'provinsi_selected' => '',
            'kota_selected' => '',
        );
        
        $this->comp('profile', $data);
    }

}