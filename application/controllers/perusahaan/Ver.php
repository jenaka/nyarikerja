<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ver extends MY_Controller {

  function __construct(){
    parent::__construct();

        if ($this->session->userdata('perusahaan_login') != 1)
        redirect(base_url() . 'login', 'refresh');
    }

    public function index() {

         $data = array(
            'title' => 'Beranda',
            'id_perusahaan' => $this->session->userdata('id_perusahaan'),
            'nama_lengkap' => $this->session->userdata('nama'),
            'industri_data' => $this->Industri_m->get_all(),
            'provinsi' => $this->Wilayah_m->get_provinsi(),
            'kota' => $this->Wilayah_m->get_kota_prov(),
            'provinsi_selected' => '',
            'kota_selected' => '',
        );
        
        $this->load->view('perusahaan/ver', $data);
    }


    public function proses(){
        // $this->form_validation->set_rules('signup-username', 'Nama Lengkap', 'trim|required');
        // $this->form_validation->set_rules('signup-email', 'Email', 'trim|required');
        // $this->form_validation->set_rules('signup-password', 'Password', 'trim|required');
        // $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        // if ($this->form_validation->run() == FALSE) {
        //     $this->index();
        // }else{

        $pegawai = array(
          'nama_lengkap' => $this->input->post('nama_lengkap',TRUE),
          'no_ktp' => $this->input->post('no_ktp',TRUE),
          'no_hp' => $this->input->post('no_hp',TRUE),
        );

        $perusahaan = array(
          'nama_perusahaan' => $this->input->post('nama_perusahaan',TRUE),
          'slug_perusahaan' => url_title($this->input->post('nama_perusahaan')),
          'id_jenis' => $this->input->post('id_jenis',TRUE),
          'id_provinsi' => $this->input->post('id_provinsi',TRUE),
          'id_kota' => $this->input->post('id_kota',TRUE),
          'web_perusahaan' => $this->input->post('web_perusahaan',TRUE),
          'deskripsi_perusahaan' => $this->input->post('deskripsi_perusahaan',TRUE),
          'alamat_perusahaan' => $this->input->post('alamat_perusahaan',TRUE),
          'status' => '1',
        );
        
        $this->Perusahaan_m->update_pegawai($this->input->post('id_perusahaan', TRUE), $pegawai);
        $this->Perusahaan_m->update_perusahaan($this->input->post('id_perusahaan', TRUE), $perusahaan);
        redirect(site_url('perusahaan/dashboard'));
      // }
    }

}