<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Profile extends MY_Controller {

  function __construct(){
    parent::__construct();

        if ($this->session->userdata('perusahaan_login') != 1)
        redirect(base_url() . 'login', 'refresh');
    }

    public function index() {

        $row = $this->Perusahaan_m->get_profile_id($this->session->userdata('id_perusahaan'));

         $data = array(
            'title' => 'Beranda',
            'nama_perusahaan' => $row->nama_perusahaan,
            'id_jenis' => $row->id_jenis,
            'deskripsi_perusahaan' => $row->deskripsi_perusahaan,
            'id_provinsi' => $row->id_provinsi,
            'id_kota' => $row->id_kota,
            'alamat_perusahaan' => $row->alamat_perusahaan,
            'logo_perusahaan' => $row->logo_perusahaan,
            'web_perusahaan' => $row->web_perusahaan,
            'industri_data' => $this->Industri_m->get_all(),
            'provinsi' => $this->Wilayah_m->get_provinsi(),
            'kota' => $this->Wilayah_m->get_kota_prov(),
            'provinsi_selected' => '',
            'kota_selected' => '',
        );
        
        $this->comp('profile', $data);
    }


    public function upload_logo(){
        
        if (!empty($_FILES['upload_logo']['name'])){
            
            $obj_row = $this->Perusahaan_m->get_profile_id($this->session->userdata('id_perusahaan'));
            $real_path = realpath(APPPATH . '../uploads/perusahaan/');
            $config['upload_path'] = $real_path;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['overwrite'] = true;
            $config['max_size'] = 6000;
            $config['file_name'] = 'Nyarikerja-'.time();
            $this->upload->initialize($config);
            if ($this->upload->do_upload('upload_logo')){
                if($obj_row->logo_perusahaan){
                    @unlink($real_path.'/'.$obj_row->logo_perusahaan); 
                    @unlink($real_path.'/thumb/'.$obj_row->logo_perusahaan);
                }
            } else{
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('msg', '<div class="alert alert-danger"> <a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Error!</strong> '.strip_tags($error['error']).' </div>');
                redirect(base_url('perusahaan/dashboard'));
                exit;
            }
            $image = array('upload_data' => $this->upload->data()); 
            $image_name = $image['upload_data']['file_name'];
            $thumb_config['image_library'] = 'gd2';
            $thumb_config['source_image']   = $real_path.'/'.$image_name;
            $thumb_config['new_image']  = $real_path.'/thumb/'.$image_name;
            $thumb_config['maintain_ratio'] = TRUE;
            $thumb_config['height'] = 50;
            $thumb_config['width']   = 70;
            
            $this->image_lib->initialize($thumb_config);
            $this->image_lib->resize();
            
            $thumb_config2['image_library'] = 'gd2';
            $thumb_config2['source_image']  = $real_path.'/'.$image_name;
            $thumb_config2['new_image'] = $real_path.'/'.$image_name;
            $thumb_config2['maintain_ratio'] = TRUE;
            $thumb_config2['height']    = 250;
            $thumb_config2['width']  = 250;
            $this->image_lib->initialize($thumb_config2);
            $this->image_lib->resize();
            
            $data = array('logo_perusahaan' => $image_name);
            $this->Perusahaan_m->update_perusahaan($obj_row->id_perusahaan, $data);
            $this->session->set_flashdata('msg', '<div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Success!</strong> Company logo uploaded successfully. </div>');
        }

        $redirect = base_url('perusahaan/profile');
        if ($this->agent->is_referral()){
            $redirect = $this->agent->referrer();   
        }
        redirect($redirect);
    }

}