<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Loker extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Loker_m');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'loker/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'loker/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'loker/index.html';
            $config['first_url'] = base_url() . 'loker/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Loker_m->total_rows($q);
        $loker = $this->Loker_m->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'loker_data' => $loker,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->comp('loker/index', $data);
    }

    public function tambah(){
        $data = array(
            'button' => 'Create',
            'action' => site_url('loker/create_action'),
		    'id_job' => set_value('id_job'),
		    'job_title' => set_value('job_title'),
		    'job_slug' => set_value('job_slug'),
		    'id_pegawai' => set_value('id_pegawai'),
		    'id_perusahaan' => set_value('id_perusahaan'),
		    'id_industri' => set_value('id_industri'),
		    'gaji' => set_value('gaji'),
		    'tgl' => set_value('tgl'),
		    'status' => set_value('status'),
		    'is_featured' => set_value('is_featured'),
		    'country' => set_value('country'),
		    'age_required' => set_value('age_required'),
		    'id_kualifikasi' => set_value('id_kualifikasi'),
		    'pengalaman' => set_value('pengalaman'),
		    'city' => set_value('city'),
		    'last_date' => set_value('last_date'),
		    'id_job_tipe' => set_value('id_job_tipe'),
		    'vacancies' => set_value('vacancies'),
		    'job_deskripsi' => set_value('job_deskripsi'),
		    'kategori' => $this->Kategori_m->get_all(),
            'sub_kategori' => $this->Kategori_m->get_sub_kategori_all(),
            'kategori_pilih' => '',
            'sub_kategori_pilih' => '',
            'provinsi' => $this->Wilayah_m->get_provinsi(),
            'kota' => $this->Wilayah_m->get_kota_prov(),
            'provinsi_selected' => '',
            'kota_selected' => '',
            'tipe_data' => $this->Tipe_m->get_all(),
            'kualifikasi_data' => $this->Kualifikasi_m->get_all(),
            'gaji_data' => $this->Gaji_m->get_all(),
            'pengalaman_data' => $this->Pengalaman_m->get_all(),
		);
        $this->comp('loker/form', $data);
    }
    
    public function simpan(){
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->tambah();
        } else {
            $data = array(
				'job_title' => $this->input->post('job_title',TRUE),
				'job_slug' =>url_title($this->input->post('job_title')),
				'id_kategori' => $this->input->post('id_kategori',TRUE),
				'id_sub_kategori' => $this->input->post('id_sub_kategori',TRUE),
				'id_perusahaan' => $this->input->post('id_perusahaan',TRUE),
				'id_provinsi' => $this->input->post('id_provinsi',TRUE),
				'id_gaji' => $this->input->post('id_gaji',TRUE),
				'pengalaman' => $this->input->post('pengalaman',TRUE),
				'id_kualifikasi' => $this->input->post('id_kualifikasi',TRUE),
				'id_kota' => $this->input->post('id_kota',TRUE),
				'id_job_tipe' => $this->input->post('id_job_tipe',TRUE),
				'job_deskripsi' => $this->input->post('job_deskripsi',TRUE),
				'keahlian' => $this->input->post('keahlian',TRUE),
				'status' => '1',
	    	);

            $this->Loker_m->simpan($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('perusahaan/loker'));
        }
    }
    
    public function edit($id){
        $row = $this->Loker_m->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Perbaharui',
	            'id_job' => set_value('id_job', $row->id_job),
				'job_title' => set_value('job_title', $row->job_title),
				'job_slug' => set_value('job_slug', $row->job_slug),
				'id_kategori' => set_value('id_kategori', $row->id_kategori),
				'id_sub_kategori' => set_value('id_sub_kategori', $row->id_sub_kategori),
				'id_perusahaan' => set_value('id_perusahaan', $row->id_perusahaan),
				'id_provinsi' => set_value('id_provinsi', $row->id_provinsi),
				'id_gaji' => set_value('id_gaji', $row->id_gaji),
				'pengalaman' => set_value('pengalaman', $row->pengalaman),
				'id_kualifikasi' => set_value('id_kualifikasi', $row->id_kualifikasi),
				'id_kota' => set_value('id_kota', $row->id_kota),
				'id_job_tipe' => set_value('id_job_tipe', $row->id_job_tipe),
				'job_deskripsi' => set_value('job_deskripsi', $row->job_deskripsi),
				'keahlian' => set_value('keahlian', $row->keahlian),
				'status' => set_value('status', $row->status),
				'last_date' => set_value('last_date', $row->last_date),
				'tgl' => set_value('tgl', $row->tgl),
			    'kategori' => $this->Kategori_m->get_all(),
	            'sub_kategori' => $this->Kategori_m->get_sub_kategori_all(),
	            'kategori_pilih' => '',
	            'sub_kategori_pilih' => '',
	            'provinsi' => $this->Wilayah_m->get_provinsi(),
	            'kota' => $this->Wilayah_m->get_kota_prov(),
	            'provinsi_selected' => '',
	            'kota_selected' => '',
	            'tipe_data' => $this->Tipe_m->get_all(),
	            'kualifikasi_data' => $this->Kualifikasi_m->get_all(),
	            'gaji_data' => $this->Gaji_m->get_all(),
	            'pengalaman_data' => $this->Pengalaman_m->get_all(),
	    	);

            $this->comp('loker/edit', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('perusahaan/loker'));
        }
    }
    
    public function update(){
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_job', TRUE));
        } else {
            $data = array(
				'job_title' => $this->input->post('job_title',TRUE),
				'job_slug' => $this->input->post('job_slug',TRUE),
				'id_pegawai' => $this->input->post('id_pegawai',TRUE),
				'id_perusahaan' => $this->input->post('id_perusahaan',TRUE),
				'id_industri' => $this->input->post('id_industri',TRUE),
				'gaji' => $this->input->post('gaji',TRUE),
				'tgl' => $this->input->post('tgl',TRUE),
				'status' => $this->input->post('status',TRUE),
				'is_featured' => $this->input->post('is_featured',TRUE),
				'country' => $this->input->post('country',TRUE),
				'age_required' => $this->input->post('age_required',TRUE),
				'id_kualifikasi' => $this->input->post('id_kualifikasi',TRUE),
				'pengalaman' => $this->input->post('pengalaman',TRUE),
				'city' => $this->input->post('city',TRUE),
				'last_date' => $this->input->post('last_date',TRUE),
				'id_job_tipe' => $this->input->post('id_job_tipe',TRUE),
				'vacancies' => $this->input->post('vacancies',TRUE),
				'job_deskripsi' => $this->input->post('job_deskripsi',TRUE),
				'contact_person' => $this->input->post('contact_person',TRUE),
				'contact_email' => $this->input->post('contact_email',TRUE),
				'contact_phone' => $this->input->post('contact_phone',TRUE),
				'viewer_count' => $this->input->post('viewer_count',TRUE),
				'ip_address' => $this->input->post('ip_address',TRUE),
	    	);

            $this->Loker_m->update($this->input->post('id_job', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('loker'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Loker_m->get_by_id($id);

        if ($row) {
            $this->Loker_m->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('loker'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('loker'));
        }
    }

    public function _rules(){
		$this->form_validation->set_rules('job_title', 'Nama Pekerjaan', 'trim|required');
		$this->form_validation->set_rules('id_kategori', 'Kategori', 'trim|required');
		$this->form_validation->set_rules('id_sub_kategori', 'Sub Kategori', 'trim|required');
		$this->form_validation->set_rules('id_provinsi', 'Provinsi', 'trim|required');
		$this->form_validation->set_rules('id_gaji', 'Gaji', 'trim|required');
		$this->form_validation->set_rules('pengalaman', 'Pengalaman', 'trim|required');
		$this->form_validation->set_rules('id_kualifikasi', 'Kualifikasi', 'trim|required');
		$this->form_validation->set_rules('id_kota', 'Kota', 'trim|required');
		$this->form_validation->set_rules('id_job_tipe', 'Tipe Pekerjaan', 'trim|required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}