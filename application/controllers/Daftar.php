<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar extends MY_Controller {
    
    function __construct() {
        parent::__construct();

      if ($this->session->userdata('seeker_login') == 1)
        redirect(base_url() . 'dashboard', 'refresh');
      if ($this->session->userdata('perusahaan_login') == 1)
        redirect(base_url() . 'perusahaan/dashboard', 'refresh');
    }

    public function index(){
      
      $this->front('auth/daftar/index');
    }
    
    public function seeker() {


        $this->front('auth/daftar/seeker');
    }

    function seeker_proses() {
      $identity = $this->input->post('identity');
      $password = $this->input->post('password');

      $credential = array('email' => $email);

      $query = $this->Pengguna_m->check_email($email);

      if ($query->num_rows() > 0) {
          $row = $query->row();

          if($this->bcrypt->check_password($password, $row->password)){

            $this->session->set_userdata('users_login', '1');
            $this->session->set_userdata('user_id', $row->id);
            $this->session->set_userdata('nama', $row->nama);
            $this->session->set_userdata('id_akses', $row->id_akses);
            $this->session->set_userdata('hak_akses', $row->hak_akses);
            $this->session->set_flashdata('success_message', 'Login Berhasil');
          
          redirect(base_url() . 'admin/dashboard', 'refresh');
      	}else{
      		$this->session->set_flashdata('error_message', 'Login Gagal! Cek Password Anda');
    		redirect(base_url() . 'admin/login', 'refresh');
      	}

      }else{
      	$this->session->set_flashdata('error_message', 'Login Gagal! Username/Email Tidak Terdaftar');
    	redirect(base_url() . 'admin/login', 'refresh');
      }
       
    }

    public function perusahaan() {

        $data = array(
          'provinsi_data' => $this->Wilayah_m->get_provinsi(),
          'id_perusahaan' => $this->Perusahaan_m->id_perusahaan(),
          'industri_data' => $this->Industri_m->get_all(),
        );

        $this->front('auth/daftar/perusahaan', $data);
    }

    public function perusahaan_proses(){

      $this->form_validation->set_rules('signup-username', 'Nama Lengkap', 'trim|required');
      $this->form_validation->set_rules('signup-email', 'Email', 'trim|required');
      $this->form_validation->set_rules('signup-password', 'Password', 'trim|required');
      $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
      if ($this->form_validation->run() == FALSE) {
            $this->index();
      }else{

        $pegawai = array(
          'id_perusahaan' => $this->input->post('id_perusahaan',TRUE),
          'email' => $this->input->post('signup-email',TRUE),
          'password' => $this->bcrypt->hash_password($this->input->post('signup-password',TRUE)),
          'nama_lengkap' => $this->input->post('signup-username',TRUE),
        );

        $perusahaan = array(
          'id_perusahaan' => $this->input->post('id_perusahaan',TRUE)
        );
        
        $this->Perusahaan_m->simpan($pegawai, $perusahaan);
        redirect(site_url('perusahaan/dashboard'));
      }
    }

}