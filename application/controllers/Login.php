<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {
    
    function __construct() {
        parent::__construct();
    }
    
    public function index() {
      if ($this->session->userdata('seeker_login') == 1)
        redirect(base_url() . 'dashboard', 'refresh');
        if ($this->session->userdata('perusahaan_login') == 1)
          redirect(base_url() . 'perusahaan/dashboard', 'refresh');
        
        $this->front('auth/login');
    }

    public function validasi() {
      $email = strtolower($this->input->post('login-email'));
      $password = $this->input->post('login-password'); 

      $credential = array('email' => $email);

      $query = $this->Seeker_m->check_email($email);

      if ($query->num_rows() > 0) {
          $row = $query->row();

          if($this->bcrypt->check_password($password, $row->password)){

            if($row->status == 1){

              $this->session->set_userdata('seeker_login', '1');
              $this->session->set_userdata('seeker_id', $row->id);
              $this->session->set_userdata('nama', $row->nama);
              $this->session->set_userdata('login_type','Seeker');
              $this->session->set_flashdata('success_message', 'Login Berhasil');
              
              redirect(base_url() . 'dashboard', 'refresh');
            }else{
              $this->session->set_flashdata('error_message', 'Login Gagal! Akun Anda Belum Aktif');
              redirect(base_url() . 'login', 'refresh');
            }
      }else{
          $this->session->set_flashdata('error_message', 'Login Gagal! Cek Password Anda');
          redirect(base_url() . 'login', 'refresh');
        }

      }

      $query = $this->Perusahaan_m->check_email($email);
      if ($query->num_rows() > 0) {
          $row = $query->row();

          if($this->bcrypt->check_password($password, $row->password)){

              $this->session->set_userdata('perusahaan_login', '1');
              $this->session->set_userdata('pegawai_id', $row->id);
              $this->session->set_userdata('id_perusahaan', $row->id_perusahaan);
              $this->session->set_userdata('nama', $row->nama_lengkap);
              $this->session->set_userdata('login_type', 'Perusahaan');
              $this->session->set_flashdata('success_message', 'Login Berhasil');

            if($row->status <= 0){
              
                redirect(base_url() . 'perusahaan/ver', 'refresh');

              }else{

                redirect(base_url() . 'perusahaan/dashboard', 'refresh');
            }
          }else{
            $this->session->set_flashdata('error_message', 'Login Gagal! Cek Password Anda');
            redirect(base_url() . 'login', 'refresh');
          }
      }
      

      else{
        $this->session->set_flashdata('error_message', 'Login Gagal! Cek Username Anda');
      redirect(base_url() . 'login', 'refresh');
      }
       
    }
    
    function logout() {
        $this->session->sess_destroy();
        $this->session->set_flashdata('logout_notification', 'logged_out');
        redirect(base_url().'', 'refresh');
    }

}