<div class="bg-white">
    <div class="hero-static content content-full bg-white invisible" data-toggle="appear">
        <!-- Header -->
        <div class="text-center">
            <h3 class="h2 font-w700 mt-50 mb-10">Welcome to Your Dashboard</h3>
            <p class="h4 font-w400 text-muted mb-0">Belum Punya Akun? <a href="<?= base_url('daftar'); ?>">Daftar Disini</a></p>
        </div>
        <!-- END Header -->

        <!-- Sign In Form -->
        <div class="row justify-content-center px-5">
            <div class="col-sm-8 col-md-6 col-xl-4">
                <!-- jQuery Validation (.js-validation-signin class is initialized in js/pages/op_auth_signin.js) -->
                <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
                <form class="js-validation-signin" action="<?= base_url('login/validasi'); ?>" method="post">
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material floating">
                                <input type="email" class="form-control" id="login-email" name="login-email">
                                <label for="login-email">Email</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material floating">
                                <input type="password" class="form-control" id="login-password" name="login-password">
                                <label for="login-password">Password</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row gutters-tiny">
                        <div class="col-12 mb-10">
                            <button type="submit" class="btn btn-block btn-hero btn-noborder btn-rounded btn-alt-primary">
                                <i class="si si-login mr-10"></i> Masuk Ke Nyarikerja
                            </button>
                        </div>
                        <div class="col-12 mb-10">
                            <a class="btn btn-block btn-noborder btn-rounded btn-alt-secondary" href="op_auth_signup.html">
                                <i class="fa fa-facebook text-muted mr-5 text-left"></i> Login Dengan Facebook
                            </a>
                        </div>
                        <div class="col-12 mb-10">
                            <a class="btn btn-block btn-noborder btn-rounded btn-alt-secondary" href="op_auth_reminder.html">
                                <i class="fa fa-google text-muted mr-5"></i> Login Dengan Gooogle
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END Sign In Form -->
    </div>
</div>
<script src="<?= base_url(); ?>assets/js/pages/op_auth_signin.js"></script>