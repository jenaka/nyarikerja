<div class="row justify-content-center py-30 px-5">
    <div class="col-md-6 col-xl-4 py-30">
        <div class="block block-rounded text-center">
            <div class="block-content block-content-full">
                <div class="item item-circle bg-danger text-danger-light mx-auto my-20">
                    <i class="fa fa-globe"></i>
                </div>
                <h3>PENCARI KERJA</h3>
            </div>
            <div class="block-content block-content-full block-content-sm bg-body-light">
                <div class="font-w600 mb-5">TEMUKAN PEKERJAAN IMPIANMU</div>
                <div class="font-size-sm text-muted">Cari pekerjaan sesuai kehalian & Buat resume online Gratis untuk memperoleh peluang kerja lebih baik.</div>
            </div>
            <div class="block-content block-content-full">
                <a class="btn btn-rounded btn-primary" href="<?= base_url('daftar/pencari-kerja'); ?>">Daftar Sekarang</a>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xl-4 py-30">
        <div class="block block-rounded text-center">
            <div class="block-content block-content-full">
                <div class="item item-circle bg-danger text-danger-light mx-auto my-20">
                    <i class="fa fa-globe"></i>
                </div>
                <h3>PERUSAHAAN</h3>
            </div>
            <div class="block-content block-content-full block-content-sm bg-body-light">
                <div class="font-w600 mb-5">TEMUKAN KANDIDAT POTENSIAL</div>
                <div class="font-size-sm text-muted">Pasang iklan lowongan agar terhubung dengan pencari kerja yang paling potensial.</div>
            </div>
            <div class="block-content block-content-full">
                <a class="btn btn-rounded btn-primary" href="<?= base_url('daftar/perusahaan'); ?>">Daftar Sekarang</a>
            </div>
        </div>
    </div>
</div>