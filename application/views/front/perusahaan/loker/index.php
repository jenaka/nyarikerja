<div class="block">
    <div class="block-header block-header-default">
        <div class="block-title">
            
        </div>
        <div class="block-options">
           
        </div>
    </div>
    <div class="block-content">
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-striped table-borderless table-vcenter">
                    <tbody>
                        <?php foreach($loker_data as $data){ ?>
                        <tr>
                            <td>
                                <a class="font-size-h5 font-w600" href="<?= base_url('lowongan-kerja/'.$data->job_slug); ?>"><?= $data->job_title; ?></a>
                                <div class="text-muted mt-5">
                                    <span class="font-w600"><i class="si si-calendar"></i> Posted on <?= date("d-m-Y", strtotime($data->tgl)); ?></span>
                                    <span class="font-w600 px-10"><i class="si si-calendar"></i> Expiring on</span>
                                </div>
                                <div class="text-muted mt-5">
                                    <a href="#" class="btn btn-primary btn-sm text-white px-30"><i class="si si-people"></i> Pelamar Kerja (0)</a>
                                    <a class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit" href="<?= base_url('perusahaan/loker/edit/'.$data->id_job); ?>"><i class="si si-note"></i></a>
                                    <a class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Hapus">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </td>
                            <td class="d-none d-xl-table-cell font-w600 font-size-sm text-muted" style="width: 120px;"></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>