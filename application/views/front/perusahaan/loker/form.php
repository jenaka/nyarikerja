<div class="block">
    <div class="block-header block-header-default">
        <div class="block-title">
            
        </div>
        <div class="block-options">
           
        </div>
    </div>
    <div class="block-content">
        <div class="row">
            <div class="col-lg-12">
                <form method="POST" action="<?= base_url('perusahaan/loker/simpan'); ?>">
                    <input type="hidden" name="id_perusahaan" value="<?= $this->session->userdata('id_perusahaan'); ?>">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label" for="job_title">Nama Pekerjaan<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" id="job_title" name="job_title" placeholder="Masukan Nama Pekerjaan..">
                            <?php echo form_error('job_title') ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label" for="id_kategori">Bidang Pekerjaan<span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <select class="form-control" id="id_kategori" name="id_kategori" placeholder="Pilih..">
                                <option value="">Pilih..</option>
                                <?php foreach ($kategori as $data) {?>
                                    <option <?php echo $kategori_pilih == $data->id_kategori ? 'selected="selected"' : '' ?> value="<?php echo $data->id_kategori ?>"><?php echo $data->nama ?></option>
                                <?php } ?>
                            </select>
                            <?php echo form_error('id_kategori') ?>
                        </div>
                        <div class="col-lg-5">
                            <select class="form-control" id="id_sub_kategori" name="id_sub_kategori" placeholder="Pilih..">
                                <option value="">Pilih..</option>
                                <?php foreach ($sub_kategori as $data) { ?>
                                <option <?php echo $sub_kategori_pilih == $data->id_kategori ? 'selected="selected"' : '' ?> class="<?php echo $data->id_kategori ?>" value="<?php echo $data->id_sub_kategori ?>"><?php echo $data->nama_sub_kategori ?></option>
                                <?php } ?>
                            </select>
                            <?php echo form_error('id_sub_kategori') ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label" for="id_job_tipe">Tipe Pekerjaan<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="form-control" name="id_job_tipe" placeholder="Pilih..">
                                <option value="">Pilih..</option>
                                <?php foreach ($tipe_data as $data) { ?>
                                <option value="<?php echo $data->id ?>"><?php echo $data->nama_tipe ?></option>
                                <?php } ?>
                            </select>
                            <?php echo form_error('id_job_tipe') ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label" for="job_title">Gaji Pekerjaan<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="form-control" name="id_gaji" placeholder="Pilih..">
                                <option value="">Pilih..</option>
                                <?php foreach ($gaji_data as $data) { ?>
                                <option value="<?php echo $data->id_gaji ?>"><?php echo $data->judul ?></option>
                                <?php } ?>
                            </select>
                            <?php echo form_error('id_gaji') ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label" for="id_kualifikasi">Kualifikasi Pendidikan<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="form-control" name="id_kualifikasi" placeholder="Pilih..">
                                <option value="">Pilih..</option>
                                <?php foreach ($kualifikasi_data as $data) { ?>
                                <option value="<?php echo $data->id ?>"><?php echo $data->nama_kualifikasi ?></option>
                                <?php } ?>
                            </select>
                            <?php echo form_error('id_kualifikasi') ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label" for="job_title">Pengalaman Kerja<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                           <select class="form-control" name="pengalaman" placeholder="Pilih..">
                                <option value="">Pilih..</option>
                                <?php foreach ($pengalaman_data as $data) { ?>
                                <option value="<?php echo $data->nama ?>"><?php echo $data->nama ?></option>
                                <?php } ?>
                            </select>
                            <?php echo form_error('pengalaman') ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label" for="id_provinsi">Lokasi<span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <select class="form-control" id="id_provinsi" name="id_provinsi" placeholder="Pilih..">
                                <option value="">Pilih..</option>
                                 <?php foreach ($provinsi as $prov) {?>
                                    <option <?php echo $provinsi_selected == $prov->id ? 'selected="selected"' : '' ?> value="<?php echo $prov->id ?>"><?php echo $prov->nama ?></option>
                                <?php } ?>
                            </select>
                            <?php echo form_error('id_provinsi') ?>
                        </div>
                        <div class="col-lg-5">
                            <select class="form-control" id="id_kota" name="id_kota" placeholder="Pilih..">
                                <option value="">Pilih..</option>
                                <?php foreach ($kota as $kot) { ?>
                                <option <?php echo $kota_selected == $kot->id_provinsi ? 'selected="selected"' : '' ?> class="<?php echo $kot->id_provinsi ?>" value="<?php echo $kot->id ?>"><?php echo $kot->nama_kota ?></option>
                                <?php } ?>
                            </select>
                            <?php echo form_error('id_kota') ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label" for="job_deskripsi">Deskripsi<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <textarea class="summernote form-control" name="job_deskripsi" placeholder="Masukan Deskripsi Pekerjaan.."></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-block btn-primary">Publikasi</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>