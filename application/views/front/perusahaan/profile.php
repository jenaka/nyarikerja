<div class="block">
    <div class="block-header block-header-default">
        <div class="block-title">
            
        </div>
        <div class="block-options">
           
        </div>
    </div>
    <div class="block-content">
        <div class="row">
            <div class="col-lg-4 uploadpic">
                <?php $image_name = ($logo_perusahaan)?'uploads/perusahaan/'.$logo_perusahaan:'assets/img/logo_placholder.png';?>
                <div class="uploadPhoto">
                    <img src="<?php echo base_url($image_name);?>" width="100%" />
                </div>
                <div class="stripBox">
                    <form name="frm_emp_up" id="frm_emp_up" method="post" action="<?php echo base_url('perusahaan/profile/upload_logo');?>" enctype="multipart/form-data">
                        <input type="file" name="upload_logo" id="upload_logo" accept="image/*" style="display:none;">
                    </form>
                    <a href="javascript:;" class="upload" title="Upload Logo"><i class="fa fa-upload"></i></a>
                    <?php if($logo_perusahaan!=''):?>
                      <a href="javascript:;" class="remove" id="remove_logo" title="Delete Logo"><i class="fa fa-trash-o"></i></a>
                    <?php endif;?>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="form-group">
                    <label for="example-input-normal">Nama Perusahaan</label>
                    <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" value="<?= $nama_perusahaan; ?>">
                </div>
                <div class="form-group">
                    <label for="wizard-validation-classic-lastname">Industri Perusahaan</label>
                    <select class="js-select2 form-control" id="id_jenis" name="id_jenis" style="width: 100%;" data-placeholder="Pilih..">
                        <option></option>
                        <?php foreach ($industri_data as $data){ ?>
                            <option <?php if($id_jenis == $data->id) echo 'selected="selected"'; ?> value="<?= $data->id; ?>"><?= $data->nama_industri;?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="wizard-validation-classic-email">Web Perusahaan</label>
                    <input class="form-control" type="text" id="web_perusahaan" name="web_perusahaan" placeholder="Masukan Web Perusahaan (Optional)" value="<?= $web_perusahaan; ?>">
                </div>
                <div class="form-group">
                    <label for="wizard-validation-classic-lastname">Provinsi</label>
                    <select class="js-select2 form-control" id="id_provinsi" name="id_provinsi" style="width: 100%;" data-placeholder="Pilih..">
                        <option></option>
                        <?php foreach ($provinsi as $prov) {?>
                            <option <?php if($id_provinsi == $prov->id) echo 'selected="selected"'; ?> <?php echo $provinsi_selected == $prov->id ? 'selected="selected"' : '' ?> value="<?php echo $prov->id ?>"><?php echo $prov->nama ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="wizard-validation-classic-lastname">Kota</label>
                    <select class="js-select2 form-control" id="id_kota" name="id_kota" style="width: 100%;" data-placeholder="Pilih..">
                        <option></option>
                        <?php foreach ($kota as $kot) { ?>
                        <option <?php if($id_kota == $kot->id) echo 'selected="selected"'; ?> <?php echo $kota_selected == $kot->id_provinsi ? 'selected="selected"' : '' ?> class="<?php echo $kot->id_provinsi ?>" value="<?php echo $kot->id ?>"><?php echo $kot->nama_kota ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="wizard-validation-classic-email">Alamat Lengkap</label>
                    <input class="form-control" type="text" id="alamat_perusahaan" name="alamat_perusahaan" placeholder="Masukan Alamat Lengkap Perusahaan" value="<?= $alamat_perusahaan; ?>">
                </div>
                <div class="form-group">
                    <label for="wizard-validation-classic-email">Deskripsi Perusahaan</label>
                    <textarea class="form-control" name="deskripsi_perusahaan" placeholder="Masukan Deskripsi Perusahaan.."><?= $deskripsi_perusahaan; ?></textarea>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(function() {
  $(".fa-upload").click(function(){
      $("#upload_logo").click();
  });
  $("#upload_logo").change(function(){
      ext_array = ['png','jpg','jpeg','gif'];   
      var ext = $('#upload_logo').val().split('.').pop().toLowerCase();
      if($.inArray(ext, ext_array) == -1) {
          alert('Invalid file provided!');
          return false;
      }
     this.form.submit();
  });
});
</script>