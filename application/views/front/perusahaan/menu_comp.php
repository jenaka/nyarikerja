<ul class="nav nav-pills flex-column push">
    <li class="nav-item">
        <a class="nav-link d-flex align-items-center justify-content-between" href="<?= base_url('perusahaan/dashboard');?>">
            <span><i class="si si-fw si-home mr-5"></i> Beranda</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link d-flex align-items-center justify-content-between" href="<?= base_url('perusahaan/profile');?>">
            <span><i class="fa fa-fw fa-bank mr-5"></i> Company Profile</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link d-flex align-items-center justify-content-between" href="<?= base_url('perusahaan/loker/tambah');?>">
            <span><i class="si si-fw si-note mr-5"></i> Pasang Lowongan kerja</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link d-flex align-items-center justify-content-between" href="<?= base_url('perusahaan/loker');?>">
            <span><i class="si si-fw si-speech mr-5"></i> Data Lowongan kerja</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link d-flex align-items-center justify-content-between" href="<?= base_url('perusahaan/loker/pelamar');?>">
            <span><i class="fa fa-fw fa-address-book-o mr-5"></i> Data Pelamar Kerja</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link d-flex align-items-center justify-content-between" href="<?= base_url();?>">
            <span><i class="fa fa-fw fa-search mr-5"></i> Cari Kandidat</span>
        </a>
    </li>
</ul>