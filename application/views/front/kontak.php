<div class="content content-full">
    <div class="row justify-content-center py-30">
        <div class="col-lg-8 col-xl-6">
            <form class="js-validation-be-contact" action="be_pages_generic_contact.html" method="post">
                <div class="form-group row">
                    <div class="col-12">
                        <label for="be-contact-name">Nama Lengkap</label>
                        <input type="text" class="form-control form-control-lg" id="be-contact-name" name="be-contact-name" placeholder="Enter your name..">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-12" for="be-contact-email">Alamat Email</label>
                    <div class="col-12">
                        <input type="email" class="form-control form-control-lg" id="be-contact-email" name="be-contact-email" placeholder="Enter your email..">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-12" for="be-contact-message">Pesan</label>
                    <div class="col-12">
                        <textarea class="form-control form-control-lg" id="be-contact-message" name="be-contact-message" rows="10" placeholder="Enter your message.."></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 text-center">
                        <button type="submit" class="btn btn-hero btn-alt-primary min-width-175">
                            <i class="fa fa-send mr-5"></i> Kirim Pesan
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>