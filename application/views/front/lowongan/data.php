<?php if(!empty($posts)): foreach($posts as $post): ?>
    <!-- Job Listing -->
	<a href="single-job-page.html" class="job-listing">

		<!-- Job Listing Details -->
		<div class="job-listing-details">

			<!-- Logo -->
			<div class="job-listing-company-logo">
				<img src="images/company-logo-01.png" alt="">
			</div>

			<!-- Details -->
			<div class="job-listing-description">
				<h3 class="job-listing-title"><?= $post['job_title']; ?></h3>

				<!-- Job Listing Footer -->
				<div class="job-listing-footer">
					<ul>
						<li><i class="icon-material-outline-business"></i> Hexagon <div class="verified-badge" title="Verified Employer" data-tippy-placement="top"></div></li>
						<li><i class="icon-material-outline-location-on"></i> San Francissco</li>
						<li><i class="icon-material-outline-business-center"></i> Full Time</li>
						<li><i class="icon-material-outline-access-time"></i> 2 days ago</li>
					</ul>
				</div>
			</div>

			<!-- Bookmark -->
			<span class="bookmark-icon"></span>
		</div>
	</a>
<?php endforeach; else: ?>
<p>Post(s) not available.</p>
<?php endif; ?>