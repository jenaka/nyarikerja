<div class="content content-full">
	<div class="row">
		<div class="col-xl-8">
			<div class="block">
	            <div class="block-content">
	                <div class="nice-copy">
                        <!-- Project Description -->
                        <h2 class="mb-10">Lowongan Kerja <?= $job_title; ?></h2>
                        <p><?= $job_deskripsi; ?></p>
                        <!-- END Project Description -->
                    </div>
	            </div>
	        </div>
		</div>
		<div class="col-xl-4">
			<div class="block block-rounded">
                <div class="block-content">
                    <a class="btn btn-block btn-hero btn-noborder btn-rounded btn-primary mb-10" href="javascript:void(0)">Lamar Pekerjaan</a>
                </div>
            </div>
			<div class="block block-rounded block-link-shadow text-center">
				<div class="block-header block-header-default">
                    <h3 class="block-title">
                        <i class="fa fa-fw fa-info"></i>
                        Tentang Perusahaan
                    </h3>
                </div>
	            <div class="block-content text-center">
	            	<div class="push">
                            <img class="img-avatar img-avatar128 img-avatar-thumb" src="<?= base_url('uploads/perusahaan/'.$logo_perusahaan); ?>" alt="" style="    border-radius: 0px;padding: 2%; box-shadow: 0 3px 12px rgba(0,0,0,0.1)">
                        </div>
                        <div class="font-w600 mb-5"><?= $nama_perusahaan; ?></div>
                        <div class="nice-copy-story"><?= $deskripsi_perusahaan; ?></div>
                    </div>
	            </div>
	        </div>
		</div>
	</div>
</div>