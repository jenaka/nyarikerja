<div class="content">
	<div class="row items-push py-30">
		<div class="col-xl-3">

		</div>
		<div class="col-xl-9">
			<div  id="jobList" class="listings-container compact-list-layout margin-top-35">
				<?php if(!empty($posts)): foreach($posts as $post): ?>            
				<!-- Job Listing -->
				<a href="<?= base_url('lowongan-kerja/'.strtolower($post['job_slug'])); ?>" class="job-listing">

					<!-- Job Listing Details -->
					<div class="job-listing-details">

						<!-- Logo -->
						<div class="job-listing-company-logo">
							<img src="<?= base_url('uploads/perusahaan/thumb/'.$post['logo_perusahaan']); ?>" alt="">
						</div>

						<!-- Details -->
						<div class="job-listing-description">
							<h3 class="job-listing-title"><?= $post['job_title']; ?></h3>

							<!-- Job Listing Footer -->
							<div class="job-listing-footer">
								<ul>
									<li><i class="si si-home"></i> <b><?= $post['nama_perusahaan']; ?></b><div class="verified-badge" title="Verified Employer" data-tippy-placement="top"></div></li>
									<li><i class="si si-pointer"></i> <?= $post['nama_kota']; ?></li>
									<li><i class="si si-briefcase"></i> <?= $post['nama_tipe']; ?></li>
									<li><i class="si si-calendar"></i> <?= time_ago($post['tgl_post']);?></li>
								</ul>
							</div>
						</div>

						<!-- Bookmark -->
						<span class="bookmark-icon"></span>
					</div>
				</a>	
				<?php endforeach; else: ?>
	            	<p>Post(s) not available.</p>
	            <?php endif; ?>
			</div>
		</div>
	</div>
</div>
<script>
function searchFilter(page_num) {
    page_num = page_num?page_num:0;
    var keywords = $('#keywords').val();
    var sortBy = $('#sortBy').val();
    $.ajax({
        type: 'POST',
        url: '<?php echo base_url(); ?>pages/ajaxPaginationData/'+page_num,
        data:'page='+page_num+'&keywords='+keywords+'&sortBy='+sortBy,
        beforeSend: function () {
            $('.loading').show();
        },
        success: function (html) {
            $('#postList').html(html);
            $('.loading').fadeOut("slow");
        }
    });
}
</script>