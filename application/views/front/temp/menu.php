<!-- Side Header -->
    <div class="content-header content-header-fullrow bg-black-op-10">
        <div class="content-header-section text-center align-parent">
            <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                <i class="fa fa-times text-danger"></i>
            </button>
            <!-- END Close Sidebar -->

            <!-- Logo -->
            <div class="content-header-item">
                <a class="font-w700" href="<?= base_url(); ?>">
                    <img src="<?= base_url('assets/img/logo_white.png'); ?>" width="130px">
                </a>
            </div>
            <!-- END Logo -->
        </div>
    </div>
    <!-- END Side Header -->

    <!-- Side Main Navigation -->
    <div class="content-side content-side-full">
        <ul class="nav-main">
            <li>
                <a class="active" href="bd_dashboard.html"><i class="si si-compass"></i>Beranda</a>
            </li>
        </ul>
    </div>
    <!-- END Side Main Navigation -->