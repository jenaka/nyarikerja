<!-- Left Section -->
<div class="content-header-section">
    <!-- Logo -->
    <div class="content-header-item">
        <a class="font-w700" href="<?= base_url(); ?>">
            <img src="<?= base_url('assets/img/logo_white.png'); ?>" width="130px">
        </a>
    </div>
    <!-- END Logo -->
</div>
<!-- END Left Section -->

<!-- Middle Section -->
<div class="content-header-section d-none d-lg-block">
    <!-- Header Navigation -->
    <ul class="nav-main-header">
        <li>
            <a href="<?= base_url('lowongan-kerja'); ?>"></i>Cari Lowongan Kerja</a>
        </li>
        <li>
            <a href="">Perusahaan</a>
        </li>
        <li>
            <a href="<?= base_url('lowongan-kerja'); ?>">Pasang Lowongan</a>
        </li>
        <li>
            <a href="<?= base_url('kontak'); ?>"></i>Kontak</a>
        </li>
    </ul>
    <!-- END Header Navigation -->
</div>
<!-- END Middle Section -->

<!-- Right Section -->
<div class="content-header-section">
    <?php if($this->session->userdata('seeker_login') == 1){ ?>
        <div class="btn-group" role="group">
            <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php echo $this->session->userdata('nama'); ?><i class="fa fa-angle-down ml-5"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right min-width-150" aria-labelledby="page-header-user-dropdown">
                <a class="dropdown-item" href="<?= base_url('beranda'); ?>" data-toggle="layout" data-action="side_overlay_toggle">
                    <i class="si si-wrench mr-5"></i> Beranda
                </a>
                <a class="dropdown-item" href="javascript:void(0)" data-toggle="layout" data-action="side_overlay_toggle">
                    <i class="si si-wrench mr-5"></i> Ganti Password
                </a>
                <!-- END Side Overlay -->

                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?= base_url('login/logout'); ?>">
                    <i class="si si-logout mr-5"></i> Keluar
                </a>
            </div>
        </div>
    <?php }else if($this->session->userdata('perusahaan_login') == 1){ ?>
        <div class="btn-group" role="group">
            <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php echo $this->session->userdata('nama'); ?><i class="fa fa-angle-down ml-5"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right min-width-150" aria-labelledby="page-header-user-dropdown">
                <a class="dropdown-item" href="<?= base_url('perusahaan/dashboard'); ?>" data-toggle="layout" data-action="side_overlay_toggle">
                    <i class="si si-home mr-5"></i> Beranda
                </a>
                <a class="dropdown-item" href="javascript:void(0)" data-toggle="layout" data-action="side_overlay_toggle">
                    <i class="si si-wrench mr-5"></i> Ganti Password
                </a>
                <!-- END Side Overlay -->

                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?= base_url('login/logout'); ?>">
                    <i class="si si-logout mr-5"></i> Keluar
                </a>
            </div>
        </div>
    <?php }else{ ?>
        <a href="<?= base_url('login'); ?>" class="btn btn-sm btn-outline-primary">Login
        </a>
        <a href="<?= base_url('daftar'); ?>" class="btn btn-sm btn-outline-primary">Daftar
        </a>
    <?php } ?>
    <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none" data-toggle="layout" data-action="sidebar_toggle">
        <i class="fa fa-navicon"></i>
    </button>
</div>