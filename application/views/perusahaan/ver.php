<!doctype html>
<!--[if lte IE 9]>     <html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en" class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <title>Codebase - Bootstrap 4 Admin Template &amp; UI Framework</title>

        <meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <!-- Open Graph Meta -->
        <meta property="og:title" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework">
        <meta property="og:site_name" content="Codebase">
        <meta property="og:description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="<?= base_url(); ?>assets/img/favicons/favicon.png">
        <link rel="icon" type="image/png" sizes="192x192" href="<?= base_url(); ?>assets/img/favicons/favicon-192x192.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url(); ?>assets/img/favicons/apple-touch-icon-180x180.png">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Codebase framework -->
        <link rel="stylesheet" id="css-main" href="<?= base_url(); ?>assets/css/codebase.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/js/plugins/select2/select2.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assets/js/plugins/select2/select2-bootstrap.min.css">
    </head>
    <body>
        <div id="page-container" class="sidebar-inverse side-scroll page-header-fixed page-header-inverse main-content-boxed">
            <!-- Sidebar -->
            <nav id="sidebar">
                <!-- Sidebar Scroll Container -->
                <div id="sidebar-scroll">
                    <!-- Sidebar Content -->
                    <div class="sidebar-content">
                        <!-- Side Header -->
                        <div class="content-header content-header-fullrow bg-black-op-10">
                            <div class="content-header-section text-center align-parent">
                                <!-- Close Sidebar, Visible only on mobile screens -->
                                <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
                                <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                                    <i class="fa fa-times text-danger"></i>
                                </button>
                                <!-- END Close Sidebar -->

                                <!-- Logo -->
                                <div class="content-header-item">
                                    <a class="link-effect font-w700" href="index.html">
                                        <i class="si si-fire text-primary"></i>
                                        <span class="font-size-xl text-dual-primary-dark">code</span><span class="font-size-xl text-primary">base</span>
                                    </a>
                                </div>
                                <!-- END Logo -->
                            </div>
                        </div>
                        <!-- END Side Header -->

                        <!-- Side Main Navigation -->
                        <div class="content-side content-side-full">

                            <ul class="nav-main">
                                <li>
                                    <a class="active" href="bd_dashboard.html"><i class="si si-compass"></i>Dashboard</a>
                                </li>
                                <li class="nav-main-heading">Layout</li>
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-puzzle"></i>Variations</a>
                                    <ul>
                                        <li>
                                            <a href="bd_variations_hero_simple_1.html">Hero Simple 1</a>
                                        </li>
                                        <li>
                                            <a href="bd_variations_hero_simple_2.html">Hero Simple 2</a>
                                        </li>
                                        <li>
                                            <a href="bd_variations_hero_simple_3.html">Hero Simple 3</a>
                                        </li>
                                        <li>
                                            <a href="bd_variations_hero_simple_4.html">Hero Simple 4</a>
                                        </li>
                                        <li>
                                            <a href="bd_variations_hero_image_1.html">Hero Image 1</a>
                                        </li>
                                        <li>
                                            <a href="bd_variations_hero_image_2.html">Hero Image 2</a>
                                        </li>
                                        <li>
                                            <a href="bd_variations_hero_image_3.html">Hero Image 3</a>
                                        </li>
                                        <li>
                                            <a href="bd_variations_hero_image_4.html">Hero Image 4</a>
                                        </li>
                                        <li>
                                            <a href="bd_variations_hero_video_1.html">Hero Video 1</a>
                                        </li>
                                        <li>
                                            <a href="bd_variations_hero_video_2.html">Hero Video 2</a>
                                        </li>
                                        <li>
                                            <a class="nav-submenu" data-toggle="nav-submenu" href="#">More Options</a>
                                            <ul>
                                                <li>
                                                    <a href="javascript:void(0)">Another Link</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Another Link</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0)">Another Link</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-main-heading">Other Pages</li>
                                <li>
                                    <a href="bd_search.html"><i class="si si-magnifier"></i>Search</a>
                                </li>
                                <li>
                                    <a href="be_pages_dashboard.html"><i class="si si-action-undo"></i>Go Back</a>
                                </li>
                            </ul>
                        </div>
                        <!-- END Side Main Navigation -->
                    </div>
                    <!-- Sidebar Content -->
                </div>
                <!-- END Sidebar Scroll Container -->
            </nav>
            <!-- END Sidebar -->

            <!-- Header -->
            <header id="page-header">
                <!-- Header Content -->
                <div class="content-header">
                    <!-- Left Section -->
                    <div class="content-header-section">
                        <!-- Logo -->
                        <div class="content-header-item">
                            <a class="link-effect font-w700 mr-5" href="index.html">
                                <i class="si si-fire text-primary"></i>
                                <span class="font-size-xl text-dual-primary-dark">code</span><span class="font-size-xl text-primary">base</span>
                            </a>
                        </div>
                        <!-- END Logo -->
                    </div>
                    <!-- END Left Section -->


                    <!-- Right Section -->
                    <div class="content-header-section">
                        <a href="<?= base_url('login/logout'); ?>" class="btn btn-circle btn-dual-secondary">
                            <i class="si si-logout"></i>
                        </a>
                        <!-- END Toggle Sidebar -->
                    </div>
                    <!-- END Right Section -->
                </div>
                <!-- END Header Content -->
            </header>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                <div class="content content-full">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <div class="js-wizard-validation-classic block">
                                <!-- Step Tabs -->
                                <ul class="nav nav-tabs nav-tabs-block nav-fill" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#wizard-validation-classic-step1" data-toggle="tab">1. Informasi Penanggung Jawab</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#wizard-validation-classic-step2" data-toggle="tab">2. Informasi Perusahaan</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#wizard-validation-classic-step3" data-toggle="tab">3. Logo Perusahaan</a>
                                    </li>
                                </ul>
                                <!-- END Step Tabs -->

                                <!-- Form -->
                                <form class="js-wizard-validation-classic-form" action="<?= base_url('perusahaan/ver/proses'); ?>" method="post">
                                    <!-- Steps Content -->
                                    <div class="block-content block-content-full tab-content" style="min-height: 265px;">
                                        <!-- Step 1 -->
                                        <div class="tab-pane active" id="wizard-validation-classic-step1" role="tabpanel">
                                            <div class="form-group">
                                                <label for="wizard-validation-classic-firstname">Nama Lengkap</label>
                                                <input class="form-control" type="text" id="nama_lengkap" name="nama_lengkap" value="<?= $nama_lengkap; ?>">
                                            </div>
                                            <div class="form-group">
                                                <label for="wizard-validation-classic-lastname">No KTP</label>
                                                <input class="form-control" type="text" id="no_ktp" name="no_ktp" placeholder="Masukan No. KTP..">
                                            </div>
                                            <div class="form-group">
                                                <label for="wizard-validation-classic-email">No. HP</label>
                                                <input class="form-control" type="text" id="no_hp" name="no_hp" placeholder="Masukan No. HP">
                                            </div>
                                        </div>
                                        <!-- END Step 1 -->

                                        <!-- Step 2 -->
                                        <div class="tab-pane" id="wizard-validation-classic-step2" role="tabpanel">
                                            <input type="hidden" name="id_perusahaan" value="<?= $id_perusahaan; ?>">
                                            <div class="form-group">
                                                <label for="wizard-validation-classic-firstname">Nama Perushaan</label>
                                                <input class="form-control" type="text" id="nama_perusahaan" name="nama_perusahaan" placeholder="Masukan Nama Perusahaan..">
                                            </div>
                                            <div class="form-group">
                                                <label for="wizard-validation-classic-lastname">Industri Perusahaan</label>
                                                <select class="js-select2 form-control" id="id_jenis" name="id_jenis" style="width: 100%;" data-placeholder="Pilih..">
                                                    <option></option>
                                                    <?php foreach ($industri_data as $data){ ?>
                                                        <option value="<?= $data->id; ?>"><?= $data->nama_industri;?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="wizard-validation-classic-email">Web Perusahaan</label>
                                                <input class="form-control" type="text" id="web_perusahaan" name="web_perusahaan" placeholder="Masukan Web Perusahaan (Optional)">
                                            </div>
                                            <div class="form-group">
                                                <label for="wizard-validation-classic-lastname">Provinsi</label>
                                                <select class="js-select2 form-control" id="id_provinsi" name="id_provinsi" style="width: 100%;" data-placeholder="Pilih..">
                                                    <option></option>
                                                    <?php foreach ($provinsi as $prov) {?>
                                                        <option <?php echo $provinsi_selected == $prov->id ? 'selected="selected"' : '' ?> value="<?php echo $prov->id ?>"><?php echo $prov->nama ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="wizard-validation-classic-lastname">Kota</label>
                                                <select class="js-select2 form-control" id="id_kota" name="id_kota" style="width: 100%;" data-placeholder="Pilih..">
                                                    <option></option> 
                                                    <?php foreach ($kota as $kot) { ?>
                                                    <option <?php echo $kota_selected == $kot->id_provinsi ? 'selected="selected"' : '' ?> class="<?php echo $kot->id_provinsi ?>" value="<?php echo $kot->id ?>"><?php echo $kot->nama_kota ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="wizard-validation-classic-email">Alamat Lengkap</label>
                                                <input class="form-control" type="text" id="alamat_perusahaan" name="alamat_perusahaan" placeholder="Masukan Alamat Lengkap Perusahaan">
                                            </div>
                                            <div class="form-group">
                                                <label for="wizard-validation-classic-email">Deskripsi Perusahaan</label>
                                                <textarea class="js-summernote form-control" name="deskripsi_perusahaan" placeholder="Masukan Deskripsi Perusahaan.."></textarea>
                                            </div>
                                        </div>
                                        <!-- END Step 2 -->

                                        <!-- Step 3 -->
                                        <div class="tab-pane" id="wizard-validation-classic-step3" role="tabpanel">
                                            <div class="form-group row">
                                                
                                            </div>
                                            <div class="form-group">
                                                <input type="file" name="logo_perusahaan" class="form-control">
                                            </div>
                                        </div>
                                        <!-- END Step 3 -->
                                    </div>
                                    <!-- END Steps Content -->

                                    <!-- Steps Navigation -->
                                    <div class="block-content block-content-sm block-content-full bg-body-light">
                                        <div class="row">
                                            <div class="col-6">
                                                <button type="button" class="btn btn-alt-secondary" data-wizard="prev">
                                                    <i class="fa fa-angle-left mr-5"></i> Sebelumnya
                                                </button>
                                            </div>
                                            <div class="col-6 text-right">
                                                <button type="button" class="btn btn-alt-secondary" data-wizard="next">
                                                    Selanjutnya <i class="fa fa-angle-right ml-5"></i>
                                                </button>
                                                <button type="submit" class="btn btn-alt-primary d-none" data-wizard="finish">
                                                    <i class="fa fa-check mr-5"></i> Simpan
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Steps Navigation -->
                                </form>
                                <!-- END Form -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <footer id="page-footer" class="opacity-0">
                <div class="content py-20 font-size-xs clearfix">
                    <div class="float-right">
                        Crafted with <i class="fa fa-heart text-pulse"></i> by <a class="font-w600" href="http://goo.gl/vNS3I" target="_blank">pixelcave</a>
                    </div>
                    <div class="float-left">
                        <a class="font-w600" href="https://goo.gl/po9Usv" target="_blank">Codebase 2.0</a> &copy; <span class="js-year-copy">2017</span>
                    </div>
                </div>
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- Codebase Core JS -->
        <script src="<?= base_url(); ?>assets/js/core/jquery.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/core/bootstrap.bundle.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/core/jquery.slimscroll.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/core/jquery.scrollLock.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/core/jquery.appear.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/core/jquery.countTo.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/core/js.cookie.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/codebase.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-chained/1.0.1/jquery.chained.min.js"></script>
        <!-- Page JS Plugins -->
        <script src="<?= base_url(); ?>assets/js/plugins/chartjs/Chart.bundle.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/plugins/select2/select2.full.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="<?= base_url(); ?>assets/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/plugins/jquery-validation/additional-methods.min.js"></script>

        <!-- Page JS Code -->
        <script src="<?= base_url(); ?>assets/js/pages/be_forms_wizard.js"></script>
        <script>
            jQuery(function () {
                // Init page helpers (BS Datepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
                Codebase.helpers(['select2']);
            });
        </script>
        <script>
            $("#id_kota").chained("#id_provinsi");
        </script>
    </body>
</html>