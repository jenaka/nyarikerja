<div class="content">
    <div class="row">
        <div class="col-lg-4">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Tambah Slider</h3>
                </div>
                <div class="block-content">
                    <form action="<?= base_url('admin/slider/simpan'); ?>" method="post" enctype="multipart/form-data">
                        <div class="form-body">
                            <div class="form-group row">
                                <label class="control-label col-md-12">Judul</label>
                                <div class="col-md-12">
                                    <input name="title" placeholder="Masukan Judul" class="form-control" type="text">
                                    <span class="text-danger"><?php echo form_error('title') ?></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-12">Sub Judul</label>
                                <div class="col-md-12">
                                    <input name="sub_title" placeholder="Masukan Sub-Judul" class="form-control" type="text">
                                    <span class="text-danger"><?php echo form_error('sub_title') ?></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-12">Status</label>
                                <div class="col-md-12">
                                    <select class="form-control" name="status">
                                        <option value="">Pilih</option>
                                        <option value="1" >Aktif</option>
                                        <option value="0" >Tidak Aktif</option>
                                    </select>
                                    <span class="text-danger"><?php echo form_error('status') ?></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label id="gambar_slider" class="control-label col-md-12">Slider</label>
                                <div class="col-md-12">
                                    <img id="gbr_slider" src="<?php echo base_url('assets/backend/img/slider-prev.png');?>" class="thumbnail  img-responsive" style="width: 100%; margin-bottom: 10px" alt="" />
                                    <input onchange="prev_slider(this);" type="file" name="path" class="form-control upload">
                                    <span class="text-danger"><?php echo form_error('path') ?></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title"></h3>
                    <div class="">
                       <button class="btn btn-primary" onclick="tambah_slider()">Tambah Slider</button>
                    </div>
                </div>
                <div class="block-content">
                    <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
                        <thead>
                            <tr>
                                <th class="text-center"></th>
                                <th>Judul</th>
                                <th>Sub-Judul</th>
                                <th>Gambar</th>
                                <th>Status</th>
                                <th>Tgl</th>
                                <th class="text-center" style="width: 15%;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($slider_data as $data){
                            ?>
                            <tr>
                                <td class="text-center"><?php echo $no++; ?></td>
                                <td class="font-w600"><?php echo $data->nama; ?></td>
                                <td class="font-w600">Rp. <?php echo number_format($data->harga,0,",","."); ?></td>
                                <td class="text-center">
                                    <a class="btn btn-sm btn-secondary" href="javascript:void(0)" title="Edit Slider" onclick="edit_slider('<?php echo $data->id_list_sim_qurban; ?>')"><i class="si si-note"></i></a>
                                    <a type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Hapus Slider" onclick="hapus_slider('<?php echo $data->id_list_sim_qurban; ?>')">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

var save_method;
var table;
var base_url = '<?php echo base_url();?>';

function tambah_slider(){
    save_method = 'ditambahkan';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $('#modal_form').modal('show');
    $('.modal-title').text('Tambah Slider');
}

function edit_slider(id){
    save_method = 'diperbaharui';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error'); 
    $('.help-block').empty(); 

    $.ajax({
        url : "<?php echo base_url(); ?>admin/simpanan/qurban/edit_slider/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id_slider"]').val(data.id_slider);
            $('[name="judul_slider"]').val(data.judul_slider);
            $('#modal_form').modal('show');
            $('.modal-title').text('Edit Album');
            $('#gbr_prev').show(); 

            if(data.gambar_slider){
                $('#gbr_slider').attr('src', base_url+'uploads/slider/'+data.gambar_slider);

            }else{
                $('#label-photo').text('Upload Photo'); // label photo upload
                $('#gbr_prev div').text('(No photo)');
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}



function save(){
      var url;
 
    if(save_method == 'ditambahkan') {
        url = "<?php echo site_url('admin/slider/tambah')?>";
    } else {
        url = "<?php echo site_url('admin/slider/update')?>";
    }
 
    var formData = new FormData($('#form')[0]);
    $.ajax({
        url : url,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data){
 
            if(data.status)
            {
                $('#modal_form').modal('hide');
                swal({
                        title: "Berhasil",
                        text: "Slider baru berhasil " + save_method,
                        timer: 3000,
                        showConfirmButton: false,
                        type: 'success'
                    });
                    window.setTimeout(function(){ 
                        location.reload();
                    } ,1500);
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); 
                }
            }
 
 
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error adding / update data');
 
        }
    });
    }


    function hapus_slider(id) {
    swal({
      title: "Anda Yakin?",
        text: "Data yang sudah dihapus tidak bisa dikembalikan",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ya, Hapus!",
        cancelButtonText: "Batal!",
        closeOnConfirm: false
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
            url: "<?php echo base_url(); ?>admin/simpanan/qurban/hapus_slider/" + id,
                type: "POST",
                dataType: "JSON",
                success: function(data) {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    swal({
                        title: "Berhasil",
                        text: "Data berhasil dihapus",
                        timer: 3000,
                        showConfirmButton: false,
                        type: 'success'
                    });
                    window.setTimeout(function(){ 
                        location.reload();
                    } ,1500);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error deleting data');
            }
        });
      } else {
        swal("Your imaginary file is safe!");
      }
    });
    }

    function prev_slider(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#gbr_slider')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-rounded mb-0">
                <div class="modal-header block-header-default">
                    <h3 class="modal-title block-title">Form Slider</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="modal-body form">
                    <form action="#" id="form" class="form-horizontal">
                        <input type="hidden" value="" name="id_slider"/> 
                        <div class="form-body">
                            <div class="form-group row">
                                <label class="control-label col-md-3">Judul</label>
                                <div class="col-md-9">
                                    <input name="nama" placeholder="Nama Slider" class="form-control" type="text">
                                    <span class="text-danger"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-3">Sub Judul</label>
                                <div class="col-md-9">
                                    <input name="nama" placeholder="Nama Slider" class="form-control" type="text">
                                    <span class="text-danger"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-3">Status</label>
                                <div class="col-md-9">
                                    <input name="nama" placeholder="Nama Slider" class="form-control" type="text">
                                    <span class="text-danger"></span>
                                </div>
                            </div>
                             <div class="form-group row">
                                <label id="gambar_slider" class="control-label col-md-3">Slider</label>
                                <div class="col-md-9">
                                    <img id="gbr_slider" src="<?php echo base_url('assets/img/slider-prev.png');?>" class="thumbnail  img-responsive" style="height: 122px;width: 300px; margin-bottom: 10px" alt="" />
                                    <input onchange="prev_slider(this);" type="file" name="img" class="form-control upload">
                                    <span class="text-danger"></span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>