<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="block block-rounded block-transparent bg-gd-sea">
                <div class="block-content">
                    <div class="py-20 text-center">
                        <h1 class="font-w700 text-white mb-10"><?php echo $title; ?></h1>
                        <h2 class="h4 font-w400 text-white-op"><?php echo $sub; ?></h2>
                    </div>
                </div>
            </div>
            <!-- Default Elements -->
            <div class="block block-rounded">
                <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#umum">Umum</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#search-photos">Photos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#search-users">Users</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#search-projects">Projects</a>
                    </li>
                </ul>
                <div class="block-content block-content-full tab-content overflow-hidden">
                    <!-- Classic -->
                    <div class="tab-pane fade show active" id="umum" role="tabpanel">
                        <div class="row">
                            
                        </div>
                    </div>
                    <!-- END Classic -->

                    <!-- Photos -->
                    <div class="tab-pane fade" id="search-photos" role="tabpanel">
                        
                    </div>
                    <!-- END Photos -->

                    <!-- Users -->
                    <div class="tab-pane fade" id="search-users" role="tabpanel">
                        
                    </div>
                    <!-- END Users -->

                    <!-- Projects -->
                    <div class="tab-pane fade" id="search-projects" role="tabpanel">
                        
                    </div>
                    <!-- END Projects -->
                </div>
            </div>
            <!-- END Default Elements -->
        </div>
    </div>
</div>