<!doctype html>
<!--[if lte IE 9]>     <html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en" class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <title>Pintasku - Growth With Us</title>

        <meta name="description" content="Pintasku - Jasa Pembuatan Web Murah Bandung">
        <meta name="author" content="Pintasku">
        <meta name="robots" content="noindex, nofollow">

        <!-- Open Graph Meta -->
        <meta property="og:title" content="Pintasku - Jasa Pembuatan Web Murah Bandung">
        <meta property="og:site_name" content="Pintasku">
        <meta property="og:description" content="Pintasku - Jasa Pembuatan Web Murah Bandung">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="assets/img/favicons/favicon.png">
        <link rel="icon" type="image/png" sizes="192x192" href="assets/img/favicons/favicon-192x192.png">
        <link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicons/apple-touch-icon-180x180.png">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Codebase framework -->
        <link rel="stylesheet" id="css-main" href="<?= base_url('assets/css/codebase.min.css'); ?>">
    </head>
    <body>

        <div id="page-container" class="main-content-boxed">
            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                <div class="bg-gd-dusk">
                    <div class="hero-static content content-full bg-white invisible" data-toggle="appear">
                        <!-- Header -->
                        <div class="py-30 px-5 text-center">
                            <a class="link-effect font-w700" href="index.html">
                                <i class="si si-fire"></i>
                                <span class="font-size-xl text-primary-dark">code</span><span class="font-size-xl">base</span>
                            </a>
                            <h1 class="h2 font-w700 mt-50 mb-10"><?php echo lang('login_heading');?></h1>
                            <h2 class="h4 font-w400 text-muted mb-0"><?php echo lang('login_subheading');?></h2>
                        </div>
                        <!-- END Header -->

                        <!-- Sign In Form -->
                        <div class="row justify-content-center px-5">
                            <div class="col-sm-8 col-md-6 col-xl-4">
                                <!-- jQuery Validation (.js-validation-signin class is initialized in js/pages/op_auth_signin.js) -->
                                <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
                                <form class="js-validation-signin" action="<?= base_url('admin/login/validate_login'); ?>" method="post">
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <div class="form-material floating">
                                                <input type="text" class="form-control" id="email" name="email">
                                                <label for="email">Email/Username</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <div class="form-material floating">
                                                <input type="password" class="form-control" id="login-password" name="password">
                                                <label for="login-password">Password</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row gutters-tiny">
                                        <div class="col-12 mb-10">
                                            <button type="submit" class="btn btn-block btn-hero btn-noborder btn-rounded btn-alt-primary">
                                                <i class="si si-login mr-10"></i> Sign In
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- END Sign In Form -->
                    </div>
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->

        <!-- Codebase Core JS -->
        <script src="<?= base_url('assets/js/core/jquery.min.js'); ?>"></script>
        <script src="<?= base_url('assets/js/core/popper.min.js'); ?>"></script>
        <script src="<?= base_url('assets/js/core/bootstrap.min.js'); ?>"></script>
        <script src="<?= base_url('assets/js/core/jquery.slimscroll.min.js'); ?>"></script>
        <script src="<?= base_url('assets/js/core/jquery.scrollLock.min.js'); ?>"></script>
        <script src="<?= base_url('assets/js/core/jquery.appear.min.js'); ?>"></script>
        <script src="<?= base_url('assets/js/core/jquery.countTo.min.js'); ?>"></script>
        <script src="<?= base_url('assets/js/core/js.cookie.min.js'); ?>"></script>
        <script src="<?= base_url('assets/js/codebase.js'); ?>"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
        <!-- Page JS Plugins -->
        <script src="<?= base_url('assets/js/plugins/jquery-validation/jquery.validate.min.js'); ?>"></script>

        <!-- Page JS Code -->
        <script src="<?= base_url('assets/assets/js/pages/op_auth_signin.js'); ?>"></script>
        <?php if ($this->session->userdata('success_message')): ?>
            <script>
                swal({
                    title: "Berhasil",
                    text: "<?php echo $this->session->userdata('success_message'); ?>",
                    timer: 3000,
                    button: false,
                    icon: 'success'
                });
            </script>
        <?php endif; ?>
        <?php if ($this->session->userdata('error_message')): ?>
            <script>
                swal({
                    title: "Oops...",
                    text: "<?php echo $this->session->userdata('error_message'); ?>",
                    timer: 3000,
                    button: false,
                    icon: 'error'
                });
            </script>
        <?php endif; ?>
    </body>
</html>