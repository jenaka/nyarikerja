<div class="content">
    <div class="block block-rounded block-transparent bg-gd-sea">
        <div class="block-content">
            <div class="py-20 text-center">
                <h1 class="font-w700 text-white mb-10"><?php echo $title; ?></h1>
                <h2 class="h4 font-w400 text-white-op"><?php echo $sub; ?></h2>
            </div>
        </div>
    </div>
    <div class="row">
    	<div class="col-lg-12">
	        <!-- Default Elements -->
	        <div class="block block-rounded">
	            <div class="block-header">
                    <h3 class="block-title"></h3>
                    <button class="btn btn-rounded btn-alt-secondary float-right" onclick="tambah()">
                        <i class="si si-plus text-primary mx-5"></i>
                        <span class="d-none d-sm-inline"> Tambah Industri</span>
                    </button>
                </div>
                <div class="block-content">
	               <table class="table table-bordered table-striped table-vcenter normal">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th>Nama Kota/Kabupaten</th>
                                <th>Jumlah Kecamatan</th>
                                <th class="text-center">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($kota_data as $data){
                            ?>
                            <tr>
                                <td class="text-center"><?= $no++; ?></td>
                                <td class="font-w600"><?= htmlspecialchars($data->nama,ENT_QUOTES,'UTF-8');?> </td>
                                <td class="font-w600"><?= htmlspecialchars($data->jum_kec,ENT_QUOTES,'UTF-8');?></td>
                                <td class="text-center">
                                    <a class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Lihat Data Kecamatan" href="<?= base_url('admin/wilayah/kecamatan/'.$data->id); ?>"><i class="si si-eye"></i></a>
                                    <a class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit Kota/Kabupaten" onclick="edit(<?= $data->id; ?>)"><i class="si si-note"></i></a>
                                    <a class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Hapus Kota/Kabupaten"  onclick="hapus(<?= $data->id; ?>)">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
	            </div>
	        </div>
	        <!-- END Default Elements -->
	    </div>
    </div>
</div>
<script type="text/javascript">

    function tambah(){
        save_method = 'add';
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();
        $('#modal_form').modal('show');
        $('#modal_title').text('Tambah Industri Pekerjaan');
    }
 
    function edit(id){
        save_method = 'update';
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();

        $.ajax({
            url : "<?php echo site_url('admin/industri/edit/')?>" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
     
                $('[name="id"]').val(data.id);
                $('[name="nama_industri"]').val(data.nama_industri);
                $('#modal_form').modal('show');
                $('#modal_title').text('Perbaharui Industri Pekerjaan');
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
            }
        });
    }

    function simpan(){
        $('#btnSave').text('Menyimpan...');
        $('#btnSave').attr('disabled',true);
        var url;
     
        if(save_method == 'add') {
            url = "<?php echo site_url('admin/industri/simpan')?>";
        } else {
            url = "<?php echo site_url('admin/industri/update')?>";
        }

        var formData = new FormData($('#form')[0]);
        $.ajax({
            url : url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function(data){
     
                if(data.status){
                    $('#modal_form').modal('hide');
                    swal({
                        title: "Berhasil",
                        text: "Data berhasil disimpan",
                        timer: 3000,
                        buttons: false,
                        icon: 'success'
                    });
                    window.setTimeout(function(){ 
                        location.reload();
                    } ,1500);
                }else{
                    for (var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                    }
                }
                $('#btnSave').text('Simpan');
                $('#btnSave').attr('disabled',false);
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error adding / update data');
                $('#btnSave').text('save');
                $('#btnSave').attr('disabled',false);
     
            }
        });
    }
 
    function hapus(id){
        if(confirm('Are you sure delete this data?'))
        {
            // ajax delete data to database
            $.ajax({
                url : "<?php echo site_url('person/ajax_delete')?>/"+id,
                type: "POST",
                dataType: "JSON",
                success: function(data)
                {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    reload_table();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error deleting data');
                }
            });
     
        }
    }

</script>

<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title" id="modal_title">Industri Pekerjaan</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                   <form id="form" class="form-horizontal">
                        <input type="hidden" value="" name="id"/> 
                        <div class="form-group">
                            <label class="col-form-label">Nama Industri Pekerjaan</label>
                            <input type="text" class="form-control" name="nama_industri" placeholder="Nama Industri Pekerjaan">
                            <div class="form-text text-danger"><?php echo form_error('nama_industri') ?></div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-primary" data-dismiss="modal" onclick="simpan()">
                    <i class="fa fa-check"></i> Simpan
                </button>
                <button type="button" class="btn btn-alt-danger" data-dismiss="modal"><i class="si si-close"></i> Batal</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->
