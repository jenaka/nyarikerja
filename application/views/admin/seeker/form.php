<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="block block-rounded block-transparent bg-gd-sea">
                <div class="block-content">
                    <div class="py-20 text-center">
                        <h1 class="font-w700 text-white mb-10"><?php echo $title; ?></h1>
                        <h2 class="h4 font-w400 text-white-op"><?php echo $sub; ?></h2>
                    </div>
                </div>
            </div>
            <!-- Default Elements -->
            <div class="block block-rounded">
                <div class="block-content">
                    <form action="<?= $action; ?>" method="post" enctype="multipart/form-data">
                        <div class="row justify-content-center">
                            <div class="col-md-6">
                                <input type="hidden" name="id_seeker" value="<?= $id_seeker; ?>">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" >Nama Lengkap</label>
                                    <div class="col-lg-8">
                                        <input type="text" id="nama" class="form-control <?php if(form_error('nama') !== ''){ echo 'is-invalid'; } ?>" name="nama" value="<?php echo $nama; ?>"  placeholder="Nama Lengkap">
                                        <div class="form-text text-danger"><?php echo form_error('nama') ?></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" >Jenis Kelamin</label>
                                    <div class="col-lg-8">
                                        <select class="form-control <?php if(form_error('jk') !== ''){ echo 'is-invalid'; } ?>" name="jk">
                                            <option value="">Pilih</option>
                                            <option value="L">Laki-laki</option>
                                            <option value="P">Perempuan</option>
                                        </select>
                                        <div class="form-text text-danger"><?php echo form_error('jk') ?></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" >Tempat Lahir</label>
                                    <div class="col-lg-8">
                                        <input type="text" id="tmp_lahir" class="form-control <?php if(form_error('tmp_lahir') !== ''){ echo 'is-invalid'; } ?>" name="tmp_lahir" value="<?php echo $tmp_lahir; ?>"  placeholder="Tempat Lahir">
                                        <div class="form-text text-danger"><?php echo form_error('tmp_lahir') ?></div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" >Lampiran Surat</label>
                                    <div class="col-lg-8">
                                        <input type="file" id="surat" class="form-control <?php if(form_error('surat') !== ''){ echo 'is-invalid'; } ?>" name="surat[]" multiple>
                                        <div class="form-text text-danger"><?php echo form_error('surat') ?></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" >Sifat Surat</label>
                                    <div class="col-lg-8">
                                        <select class="form-control <?php if(form_error('sifat_surat') !== ''){ echo 'is-invalid'; } ?>" name="sifat_surat">
                                            <option value="">Pilih</option>
                                            <option value="Amat Segera">Amat Segera</option>
                                            <option value="Segera">Segera</option>
                                            <option value="Biasa">Biasa</option>
                                        </select>
                                        <div class="form-text text-danger"><?php echo form_error('sifat_surat') ?></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" >Media</label>
                                    <div class="col-lg-8">
                                        <select class="form-control <?php if(form_error('media') !== ''){ echo 'is-invalid'; } ?>" name="media">
                                            <option value="">Pilih</option>
                                            <option value="Hardcopy">Hardcopy</option>
                                            <option value="Softcopy">Softcopy</option>
                                        </select>
                                        <div class="form-text text-danger"><?php echo form_error('media') ?></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" >Keterangan</label>
                                    <div class="col-lg-8">
                                        <textarea class="form-control" name="keterangan" rows="7" placeholder="Keterangan Tambahan (Jika ada)"></textarea>
                                        <div class="form-text text-danger"><?php echo form_error('keterangan') ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center" style="padding-top: 30px;padding-bottom: 25px;">
                            <div class="col-lg-6">
                                <button type="submit" class="btn btn-primary btn-lg btn-block">Simpan</button>
                            </div>
                            <div class="col-lg-6">
                                <input type="reset" class="btn btn-danger btn-lg btn-block" value="Reset"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Default Elements -->
        </div>
    </div>
</div>