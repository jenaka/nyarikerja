<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="block block-rounded block-transparent bg-gd-sea">
                <div class="block-content">
                    <div class="py-20 text-center">
                        <h1 class="font-w700 text-white mb-10"><?php echo $title; ?></h1>
                        <h2 class="h4 font-w400 text-white-op"><?php echo $sub; ?></h2>
                    </div>
                </div>
            </div>
            <!-- Default Elements -->
            <div class="block block-rounded">
                <div class="block-header">
                    <h3 class="block-title"><?php echo $title; ?></h3>
                    <div class="">
                        <button class="btn btn-primary" type="button" onclick="tambah()">Tambah Foto</button>
                    </div>
                </div>
                <div class="block-content">
                    <?php if($galeri_data == NULL || $galeri_data == ''){ ?>
                        <div class="row">
                            <div class="col-12">
                                <div class="font-size-h3 font-w600 py-30 mb-20 text-center">
                                    <span class="font-w700">Tidak Ada Foto</span>
                                </div>
                            </div>
                        </div>
                    <?php }else{ ?>
                    <div class="row items-push js-gallery img-fluid-100">
                        <?php
                        foreach ($galeri_data as $data){
                        ?>
                        <div class="col-md-6 col-lg-4 col-xl-3 push">
                            <a class="img-link img-link-zoom-in img-thumb img-lightbox" href="<?= base_url($data->path); ?>">
                                <img class="img-fluid" src="<?= base_url($data->path); ?>" alt="" style="width: 350px;height: 200px;">
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                <?php } ?>
                </div>
            </div>
            <!-- END Default Elements -->

        </div>
    </div>
</div>
<script type="text/javascript">
    function tambah(){
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty(); 
        $('#modal_form').modal('show');
        $('#modal-title').text('Tambah Album');
    }
 
    function hapus(id){
        if(confirm('Are you sure delete this data?'))
        {
            // ajax delete data to database
            $.ajax({
                url : "<?php echo site_url('person/ajax_delete')?>/"+id,
                type: "POST",
                dataType: "JSON",
                success: function(data)
                {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    reload_table();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error deleting data');
                }
            });
     
        }
    }
</script>

<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title" id="modal-title">Edit Kategori</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                   <form action="<?= base_url('admin/galeri/upload_foto'); ?>" id="form" class="form-horizontal" method="post" enctype="multipart/form-data">
                        <input type="hidden" value="<?= $id_album; ?>" name="id_album"/>
                        <div class="form-group row">
                            <div class="col-12">
                                <input type="file" name="userFiles[]" multiple>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <button type="submit" class="btn btn-alt-primary btn-block">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div><!-- /.modal -->


