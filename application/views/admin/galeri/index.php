<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="block block-rounded block-transparent bg-gd-sea">
                <div class="block-content">
                    <div class="py-20 text-center">
                        <h1 class="font-w700 text-white mb-10"><?php echo $title; ?></h1>
                        <h2 class="h4 font-w400 text-white-op"><?php echo $sub; ?></h2>
                    </div>
                </div>
            </div>
            <!-- Default Elements -->
            <div class="block block-rounded">
                <div class="block-header block-header-default">
                    <h3 class="block-title"><?php echo $title; ?></h3>
                    <div class="">
                        <button class="btn btn-rounded btn-alt-secondary float-right" type="button" onclick="tambah()">
                            <i class="si si-plus mx-5"></i>
                            <span class="d-none d-sm-inline"> Tambah Album</span>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <div class="row items-push js-gallery">
                        <?php
                        foreach ($album_data as $data){
                        ?>
                        <div class="col-md-4 col-lg-3 animated fadeIn">
                            <div class="options-container fx-item-zoom-in fx-overlay-slide-down">
                                <img class="img-fluid options-item" src="<?= base_url($data->photo); ?>" alt="" style="width: 350px;height: 150px;">
                                <div class="options-overlay bg-black-op-75">
                                    <div class="options-overlay-content">
                                        <h3 class="h4 text-white mb-5"><?= $data->judul; ?></h3>
                                        <h4 class="h6 text-white-op mb-15"><?= $data->jum_foto; ?> Foto</h4>
                                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-primary min-width-75" href="<?= base_url('admin/galeri/detail/'.$data->id_album); ?>">
                                            <i class="fa fa-search-plus"></i> Detail
                                        </a>
                                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-success min-width-75" href="javascript:void(0)" onclick="edit(<?= $data->id_album; ?>)"><i class="fa fa-pencil"></i> Edit</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <!-- END Default Elements -->

        </div>
    </div>
</div>
<script type="text/javascript">
    function tambah(){
        save_method = 'add';
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty(); 
        $('#modal_form').modal('show');
        $('#modal-title').text('Tambah Album');
        $('#photo-preview').hide();
        $('#label-photo').text('Upload Photo');
    }
 
    function edit(id){
        save_method = 'update';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
     
     
        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo site_url('admin/galeri/album_edit')?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data){
     
                $('[name="id_album"]').val(data.id_album);
                $('[name="judul"]').val(data.judul);
                $('#modal_form').modal('show');
                $('.modal-title').text('Edit Person');
                $("#gbr_slider").attr("src","<?php echo base_url();?>"+data.photo);
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
            }
        });
    }
 
    function save(){
        $('#btnSave').text('Menyimpan...'); //change button text
        $('#btnSave').attr('disabled',true); //set button disable 
        var url;
     
        if(save_method == 'add') {
            url = "<?php echo site_url('admin/galeri/album_simpan')?>";
        } else {
            url = "<?php echo site_url('admin/galeri/album_update')?>";
        }
     
        // ajax adding data to database
     
        var formData = new FormData($('#form')[0]);
        $.ajax({
            url : url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function(data){
     
                if(data.status){
                    $('#modal_form').modal('hide');
                    reload_table();
                }else{
                    for (var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }

                $('#btnSave').text('Simpan');
                $('#btnSave').attr('disabled',false);
     
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error adding / update data');
                $('#btnSave').text('Simpan'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable 
            }
        });
    }
 
    function delete_person(id){
        if(confirm('Are you sure delete this data?'))
        {
            // ajax delete data to database
            $.ajax({
                url : "<?php echo site_url('person/ajax_delete')?>/"+id,
                type: "POST",
                dataType: "JSON",
                success: function(data)
                {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    reload_table();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error deleting data');
                }
            });
     
        }
    }

    function prev_gambar(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#gbr_slider')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title" id="modal-title">Edit Kategori</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                   <form action="#" id="form" class="form-horizontal">
                        <input type="hidden" value="" name="id_album"/> 
                        <div class="form-group">
                            <label class="col-form-label">Judul Album</label>
                            <input type="text" class="form-control" name="judul" placeholder="Judul Album">
                            <div class="form-text text-danger"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Cover Album</label>
                            <div class="col-lg-12">
                                <img id="gbr_slider" src="<?php echo base_url('assets/backend/img/slider-prev.png');?>" class="thumbnail  img-responsive" style="width: 100%; margin-bottom: 10px" alt="" />
                            </div>
                            <input type="file" class="form-control" name="photo" onchange="prev_gambar(this);">
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" class="btn btn-alt-primary" data-dismiss="modal" onclick="save()">
                    <i class="fa fa-check"></i> Simpan
                </button>
                <button type="button" class="btn btn-alt-danger" data-dismiss="modal"><i class="si si-close"></i> Batal</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->

