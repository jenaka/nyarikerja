<div class="content">
    <div class="row">
    	<div class="col-lg-12">
    		<div class="block block-rounded block-transparent bg-gd-sea">
                <div class="block-content">
                    <div class="py-20 text-center">
                        <h1 class="font-w700 text-white mb-10"><?php echo $title; ?></h1>
                        <h2 class="h4 font-w400 text-white-op"><?php echo $sub; ?></h2>
                    </div>
                </div>
            </div>
	        <!-- Default Elements -->
	        <div class="block block-rounded">
	            <div class="block-header">
                    <h3 class="block-title"></h3>
                </div>
	            <div class="block-content">
	                <form action="<?= $action; ?>" method="post">
	                	<div class="row justify-content-center">
	                		<div class="col-md-6">
	                			<input type="hidden" name="id" value="<?= $id;?>">
	                			<div class="form-group row">
		                            <label class="col-lg-4 col-form-label" >Nama Lengkap</label>
		                            <div class="col-lg-8">
		                                <input type="text" id="nama" class="form-control <?php if(form_error('nama') !== ''){ echo 'is-invalid'; } ?>" name="nama" value="<?php echo $nama; ?>"  placeholder="Nama Lengkap">
		                                <div class="form-text text-danger"><?php echo form_error('nama') ?></div>
		                            </div>
		                        </div>
		                        <div class="form-group row">
		                            <label class="col-lg-4 col-form-label" >Username</label>
		                            <div class="col-lg-8">
		                                <input type="text" id="username" class="form-control <?php if(form_error('username') !== ''){ echo 'is-invalid'; } ?>" name="username" value="<?php echo $username; ?>"  placeholder="Username">
		                                <div class="form-text text-danger"><?php echo form_error('username') ?></div>
		                            </div>
		                        </div>
		                         <div class="form-group row">
		                            <label class="col-lg-4 col-form-label" >Email</label>
		                            <div class="col-lg-8">
		                                <input type="email" id="email" class="form-control <?php if(form_error('email') !== ''){ echo 'is-invalid'; } ?>" name="email" value="<?php echo $email; ?>"  placeholder="Email">
		                                <div class="form-text text-danger"><?php echo form_error('email') ?></div>
		                            </div>
		                        </div>
		                        <div class="form-group row">
		                            <label class="col-lg-4 col-form-label" >Jabatan</label>
		                            <div class="col-lg-8">
	                                    <select class="form-control <?php if(form_error('id_akses') !== ''){ echo 'is-invalid'; } ?>" name="id_akses">
		                                	<option value="">Pilih</option>
					                            <?php
					                            foreach ($jabatan_data as $data) {
					                                ?>
					                                <option <?php if($id_akses == $data->id){ echo 'selected="selected"'; } ?> value="<?php echo $data->id ?>"><?php echo $data->nama ?></option>
					                                <?php
					                            }
					                            ?>
		                                </select>
		                                <div class="form-text text-danger"><?php echo form_error('id_akses') ?></div>
	                                </div>
		                        </div>
		                    	<div class="row justify-content-center" style="padding-top: 30px;padding-bottom: 25px;">
		                            <div class="col-lg-6">
		                                <button type="submit" class="btn btn-primary btn-lg btn-block">Simpan</button>
		                            </div>
		                            <div class="col-lg-6">
		                                <a class="btn btn-danger btn-lg btn-block" href="<?php echo base_url('admin/pengguna');?>">Batal</a>
		                            </div>
			                    </div>
	                		</div>
	                    </div>
                    </form>
	            </div>
	        </div>
	        <!-- END Default Elements -->
	    </div>
    </div>
</div>