<nav id="sidebar">
    <!-- Sidebar Scroll Container -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <div class="sidebar-content">
            <!-- Side Header -->
            <div class="content-header content-header-fullrow px-15" style="background-color: #2d3238;">
                <!-- Mini Mode -->
                <div class="content-header-section sidebar-mini-visible-b">
                    <!-- Logo -->
                    <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
                        <span class="text-dual-primary-dark">c</span><span class="text-primary">b</span>
                    </span>
                    <!-- END Logo -->
                </div>
                <!-- END Mini Mode -->

                <!-- Normal Mode -->
                <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                    <!-- Close Sidebar, Visible only on mobile screens -->
                    <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
                    <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                        <i class="fa fa-times text-danger"></i>
                    </button>
                    <!-- END Close Sidebar -->

                    <!-- Logo -->
                    <div class="content-header-item" >
                        <a class="link-effect font-w700" href="<?= base_url('admin'); ?>">
                            <i class="si si-fire text-primary"></i>
                            <span class="font-size-xl text-dual-primary-dark"></span><span class="font-size-xl text-primary">Admin Panel</span>
                        </a>
                    </div>
                    <!-- END Logo -->
                </div>
                <!-- END Normal Mode -->
            </div>
            <!-- END Side Header -->

            <!-- Side User -->
            
            <!-- END Side User -->

            <!-- Side Navigation -->
            <div class="content-side content-side-full">
                <ul class="nav-main">
                    <li>
                        <a href="<?php echo base_url('admin/dashboard');?>" class="<?php if($this->uri->segment(2)=="dashboard"){echo 'active';}?>"><i class="si si-compass"></i><span class="sidebar-mini-hide">Beranda</span></a>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-doc"></i><span class="sidebar-mini-hide">Lowongan Kerja</span></a>
                        <ul>
                            <li>
                                <a href="<?php echo base_url('admin/loker/tambah');?>">Tambah Lowongan Kerja</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/loker');?>">Data Lowongan kerja</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/kategori');?>">Kategori</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-briefcase"></i><span class="sidebar-mini-hide">Perusahaan</span></a>
                        <ul>
                            <li>
                                <a href="<?php echo base_url('admin/perusahaan/tambah');?>">Tambah Perusahaan</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/perusahaan');?>">Data Perusahaan</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-users"></i><span class="sidebar-mini-hide">Job Seeker</span></a>
                        <ul>
                            <li>
                                <a href="<?php echo base_url('admin/seeker/tambah');?>">Tambah Job Seeker</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/seeker');?>">Data Job Seeker</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo base_url('admin/industri');?>" class="<?php if($this->uri->segment(2)=="industri"){echo 'active';}?>"><i class="si si-directions"></i><span class="sidebar-mini-hide">Industri</span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('admin/tipe');?>" class="<?php if($this->uri->segment(2)=="tipe"){echo 'active';}?>"><i class="si si-directions"></i><span class="sidebar-mini-hide">Tipe Pekerjaan</span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('admin/gaji');?>" class="<?php if($this->uri->segment(2)=="gaji"){echo 'active';}?>"><i class="si si-directions"></i><span class="sidebar-mini-hide">Gaji Pekerjaan</span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('admin/pengalaman');?>" class="<?php if($this->uri->segment(2)=="pengalaman"){echo 'active';}?>"><i class="si si-directions"></i><span class="sidebar-mini-hide">Pengalaman</span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('admin/keahlian');?>" class="<?php if($this->uri->segment(2)=="keahlian"){echo 'active';}?>"><i class="si si-directions"></i><span class="sidebar-mini-hide">Keahlian</span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('admin/kualifikasi');?>" class="<?php if($this->uri->segment(2)=="kualifikasi"){echo 'active';}?>"><i class="si si-graduation"></i><span class="sidebar-mini-hide">Kualifikasi</span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('admin/wilayah');?>" class="<?php if($this->uri->segment(2)=="wilayah"){echo 'active';}?>"><i class="si si-compass"></i><span class="sidebar-mini-hide">Wilayah</span></a>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-user"></i><span class="sidebar-mini-hide">Pengguna</span></a>
                        <ul>
                            <li>
                                <a href="<?php echo base_url('admin/pengguna/tambah');?>">Tambah Pengguna</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin/pengguna');?>">Data Pengguna</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo base_url('admin/galeri');?>" class="<?php if($this->uri->segment(2)=="tagihan"){echo 'active';}?>"><i class="si si-envelope-letter"></i><span class="sidebar-mini-hide">Kontak Pesan</span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('admin/settings');?>" class="<?php if($this->uri->segment(2)=="tagihan"){echo 'active';}?>"><i class="fa fa-gear"></i><span class="sidebar-mini-hide">Pengaturan</span></a>
                    </li>
                </ul>
            </div>
            <!-- END Side Navigation -->
        </div>
        <!-- Sidebar Content -->
    </div>
    <!-- END Sidebar Scroll Container -->
</nav>