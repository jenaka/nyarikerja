<div class="content">
    <div class="block block-rounded block-transparent bg-gd-sea">
        <div class="block-content">
            <div class="py-20 text-center">
                <h1 class="font-w700 text-white mb-10"><?php echo $title; ?></h1>
                <h2 class="h4 font-w400 text-white-op"><?php echo $sub; ?></h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="block block-themed block-rounded">
                <div class="block-content">
                    <form action="<?php echo base_url('admin/media/kategori_simpan'); ?>" method="post" id="form">
                        <input type="hidden" value="" name="id" />
                        <div class="form-group">
                            <label class="col-form-label">Judul Kategori</label>
                            <input type="text" class="form-control" name="judul_kategori" placeholder="Judul Kategori" value="<?php echo $judul_kategori; ?>">
                            <div class="form-text text-danger"><?php echo form_error('judul_kategori') ?></div>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Deskripsi Kategori</label>
                            <input type="text" class="form-control" name="deskripsi" placeholder="Deskripsi Kategori" value="<?php echo $deskripsi; ?>">
                            <div class="form-text text-danger"><?php echo form_error('deskripsi') ?></div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-alt-primary btn-block">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    	<div class="col-lg-8">
	        <!-- Default Elements -->
	        <div class="block block-themed block-rounded">
	            <div class="block-content">
	               <table class="table table-bordered table-striped table-vcenter tabelnya">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th>Judul</th>
                                <th>Deskripsi</th>
                                <th class="text-center"></th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
                        	$no = 1;
					        foreach ($kategori_data as $data){
					        ?>
                            <tr>
                                <td class="text-center"><?= $no++; ?></td>
                                <td class="font-w600"><?= htmlspecialchars($data->judul_kategori,ENT_QUOTES,'UTF-8');?> </td>
                                <td class="font-w600"><?= htmlspecialchars($data->deskripsi,ENT_QUOTES,'UTF-8');?> </td>
                                <td class="text-center">
                                    <a class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit Kategori" onclick="edit(<?= $data->id_kategori; ?>)"><i class="si si-note"></i></a>
                                    <a class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Hapus Kategori"  onclick="hapus(<?= $data->id_kategori; ?>)">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
	            </div>
	        </div>
	        <!-- END Default Elements -->
	    </div>
    </div>
</div>
<script type="text/javascript">
 
function edit(id){
    save_method = 'update';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();

    $.ajax({
        url : "<?php echo site_url('admin/media/kategori_edit')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
 
            $('[name="id"]').val(data.id);
            $('[name="judul_kategori"]').val(data.judul_kategori);
            $('[name="deskripsi"]').val(data.deskripsi);
            $('#modal_form').modal('show');
            $('.modal-title').text('Edit Person');
 
            if(data.photo)
            {
                $('#label-photo').text('Change Photo'); // label photo upload
                $('#photo-preview div').html('<img src="'+base_url+'upload/'+data.photo+'" class="img-responsive">'); // show photo
                $('#photo-preview div').append('<input type="checkbox" name="remove_photo" value="'+data.photo+'"/> Remove photo when saving'); // remove photo
 
            }
            else
            {
                $('#label-photo').text('Upload Photo'); // label photo upload
                $('#photo-preview div').text('(No photo)');
            }
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
 
function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}
 
function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
 
    if(save_method == 'add') {
        url = "<?php echo site_url('person/ajax_add')?>";
    } else {
        url = "<?php echo site_url('person/ajax_update')?>";
    }
 
    // ajax adding data to database
 
    var formData = new FormData($('#form')[0]);
    $.ajax({
        url : url,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data)
        {
 
            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
        }
    });
}
 
function delete_person(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('person/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
 
    }
}

</script>

<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Edit Kategori</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                   <form action="#" id="form" class="form-horizontal">
                        <input type="hidden" value="" name="id"/> 
                        <div class="form-group">
                            <label class="col-form-label">Judul Kategori</label>
                            <input type="text" class="form-control" name="judul_kategori" placeholder="Judul Kategori" value="<?php echo $judul_kategori; ?>">
                            <div class="form-text text-danger"><?php echo form_error('judul_kategori') ?></div>
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">Deskripsi Kategori</label>
                            <input type="text" class="form-control" name="deskripsi" placeholder="Deskripsi Kategori" value="<?php echo $deskripsi; ?>">
                            <div class="form-text text-danger"><?php echo form_error('deskripsi') ?></div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-primary" data-dismiss="modal" onclick="save()">
                    <i class="fa fa-check"></i> Simpan
                </button>
                <button type="button" class="btn btn-alt-danger" data-dismiss="modal"><i class="si si-close"></i> Batal</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->
