<div class="content" style="max-width: 100%">
	<form action="<?= $action; ?>" method="post" enctype="multipart/form-data">
		<input type="hidden" name="id_berita" value="<?= $id_berita; ?>">
	    <div class="row">
	    	<div class="col-lg-8">
		        <div class="block">
		            <div class="block-content">
	                	<div class="row justify-content-center">
	                		<div class="col-lg-12">
	                			<div class="form-group">
		                            <label class="col-form-label">Judul</label>
		                            <input type="text" class="form-control" name="judul" placeholder="Judul" value="<?php echo $judul; ?>">
		                            <div class="form-text text-danger"><?php echo form_error('judul') ?></div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-form-label">Slug (Jika dikosongkan maka akan otomatis terisi)</label>
		                            <input type="text" class="form-control" name="slug" placeholder="slug" value="<?php echo $slug; ?>">
		                            <div class="form-text text-danger"><?php echo form_error('slug') ?></div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-form-label">Kategori</label>
		                            <select class="form-control" name="id_kategori">
		                            	<option value="">Pilih Kategori</option>
		                            	<?php foreach ($kategori_data as $data) { ?>
		                            		<option value="<?= $data->id_kategori; ?>" <?php if($id_kategori == $data->id_kategori){ echo 'selected="selected"'; } ?>><?= $data->judul_kategori; ?></option>
		                            	<?php }?>
		                            </select>
		                            <div class="form-text text-danger"><?php echo form_error('id_kategori') ?></div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-form-label">Konten</label>
		                            <textarea id="isi_berita" name="konten" rows="10" cols="80"><?= $konten; ?></textarea>
		                        </div>
	                		</div>
	                    </div>
		            </div>
		        </div>
		    </div>
		    <div class="col-lg-4">
		    	<div class="block">
		            <div class="block-content">
	                	<div class="row justify-content-center">
	                		<div class="col-lg-12">
	                			<div class="form-group">
		                            <label class="col-form-label">Gambar</label>
		                            <div class="col-lg-12">
			                            <img id="gbr_slider" src="<?php echo base_url(); if($gambar == ''){ echo 'assets/backend/img/slider-prev.png'; }else{ echo $gambar; }?>" class="thumbnail  img-responsive" style="width: 100%; margin-bottom: 10px" alt="" />
			                        </div>
			                        <input type="file" class="form-control" name="photo" onchange="prev_gambar(this);" accept="image/*">
		                        </div>
	                			<div class="form-group">
		                            <label class="col-form-label">Status</label>
		                            <select class="form-control" name="status">
		                            	<option value="1">Publikasi</option>
		                            	<option value="0">Draft</option>
		                            </select>
		                            <div class="form-text text-danger"><?php echo form_error('status') ?></div>
		                        </div>
		                        <div class="form-group row">
		                        	<div class="col-lg-12">
		                            	<button type="submit" class="btn btn-primary btn-block">Simpan</button>
		                            </div>
		                        </div>
	                		</div>
	                    </div>
		            </div>
		        </div>
		    </div>
		</div>
	</form>
</div>
<script type="text/javascript">

	function prev_gambar(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#gbr_slider')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
	$(function () {
	   var editor = CKEDITOR.replace('isi_berita', {
	            filebrowserBrowseUrl: '<?php echo base_url(); ?>assets/ckfinder/ckfinder.html',
	            filebrowserImageBrowseUrl: '<?php echo base_url(); ?>assets/ckfinder/ckfinder.html?type=Images',
	            filebrowserFlashBrowseUrl: '<?php echo base_url(); ?>assets/ckfinder/ckfinder.html?type=Flash',
	            filebrowserUploadUrl: '<?php echo base_url(); ?>assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
	            filebrowserImageUploadUrl: '<?php echo base_url(); ?>assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	            filebrowserFlashUploadUrl: '<?php echo base_url(); ?>assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
	        });
	        CKFinder.setupCKEditor(editor, '../');
	});
</script>