<div class="content">
    <div class="row">
    	<div class="col-lg-12">
            <div class="block block-rounded block-transparent bg-gd-sea">
                <div class="block-content">
                    <div class="py-20 text-center">
                        <h1 class="font-w700 text-white mb-10"><?php echo $title; ?></h1>
                        <h2 class="h4 font-w400 text-white-op"><?php echo $sub; ?></h2>
                    </div>
                </div>
            </div>
	        <!-- Default Elements -->
	        <div class="block block-rounded">
	            <div class="block-content">
                    <div class="table-responsive">
    	                <table class="table table-bordered table-striped table-vcenter" id="data_media">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Judul Berita</th>
                                    <th>Kategori</th>
                                    <th>Tanggal</th>
                                    <th width="150px"></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
	            </div>
	        </div>
	        <!-- END Default Elements -->
	    </div>
    </div>
</div>

<script type="text/javascript">

    var table;

    $(document).ready(function() {

        //datatables
        table = $('#data_media').dataTable({ 

            "processing": true, 
            "serverSide": true,
            "order": [],

            "ajax": {
                "url": "<?php echo site_url('admin/media/media_list')?>",
                "type": "POST",
                "data": function ( data ) {
                    data.id_berita = $('#id_berita').val();
                    data.judul_berita = $('#judul_berita').val();
                    data.judul_kategori = $('#judul_kategori').val();
                    data.tgl_berita = $('#tgl_berita').val();
                }
            },

            "language":{
                "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Indonesian.json",
                "sEmptyTable": "Tidak ada data di database"
            },

            "columnDefs": [
            { 
                "targets": [ 0, 2, 4],
                "orderable": false, //set not orderable
            },
            ],

        });

        $('#btn-filter').click(function(){ //button filter event click
            table.ajax.reload();  //just reload table
        });
        $('#btn-reset').click(function(){ //button reset event click
            $('#form-filter')[0].reset();
            table.ajax.reload();  //just reload table
        });

    });

    function hapus(id) {
    swal({
      title: "Anda Yakin?",
      text: false,
      icon: "warning",
      buttons: ["Batal!", "Hapus!"],
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
            url: "<?php echo base_url(); ?>surat/masuk/hapus/" + id,
                type: "POST",
                dataType: "JSON",
                success: function(data) {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    swal({
                        title: "Berhasil",
                        text: "Data berhasil dihapus",
                        timer: 3000,
                        buttons: false,
                        icon: 'success'
                    });
                    window.setTimeout(function(){ 
                        location.reload();
                    } ,1500);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error deleting data');
            }
        });
      } else {
        window.setTimeout(function(){ 
            location.reload();
        } ,1500);
      }
    });
    }
</script>