<?php 

class MY_Controller extends CI_Controller 
{

    function __construct(){
        parent::__construct();

  }

   function admin($content, $data = NULL){

        $data['header'] = $this->load->view('admin/temp/header', $data, TRUE);
        $data['menu'] = $this->load->view('admin/temp/menu', $data, TRUE);
        $data['content'] = $this->load->view('admin/'.$content, $data, TRUE);
        $data['footer'] = $this->load->view('admin/temp/footer', $data, TRUE);
        
        $this->load->view('admin/index', $data);
    }

    function comp($content, $data = NULL){

        $data['header'] = $this->load->view('front/temp/header', $data, TRUE);
        $data['menu'] = $this->load->view('front/temp/menu', $data, TRUE);
        $data['menu_header'] = $this->load->view('front/temp/menu_header', $data, TRUE);
        $data['menu_comp'] = $this->load->view('front/perusahaan/menu_comp', $data, TRUE);
        $data['content'] = $this->load->view('front/perusahaan/'.$content, $data, TRUE);
        $data['footer'] = $this->load->view('front/temp/footer', $data, TRUE);
        
        $this->load->view('front/perusahaan/index', $data);
    }

    function front($content, $data = NULL){

        $data['header'] = $this->load->view('front/temp/header', $data, TRUE);
        $data['menu'] = $this->load->view('front/temp/menu', $data, TRUE);
        $data['menu_header'] = $this->load->view('front/temp/menu_header', $data, TRUE);
        $data['content'] = $this->load->view('front/'.$content, $data, TRUE);
        $data['footer'] = $this->load->view('front/temp/footer', $data, TRUE);
        
        $this->load->view('front/index', $data);
    }

}