<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Seeker_m extends CI_Model
{

    public $table = 'seeker';
    public $id = 'id_seeker';
    public $order = 'DESC';

    public function __construct(){
        parent::__construct();
    }


    public function check_email($email){
        $this->db->select('id_seeker, nama, email, password, foto, status');
        $this->db->from($this->table);
        $this->db->where('email',$email);
        return $this->db->get(); 
    }

    public function get_all(){
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    public function get_by_id($id){
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    public function total_rows($q = NULL) {
        $this->db->like('id_seeker', $q);
    	$this->db->or_like('nama', $q);
    	$this->db->or_like('username', $q);
    	$this->db->or_like('email', $q);
    	$this->db->or_like('password', $q);
    	$this->db->or_like('tmp_lahir', $q);
    	$this->db->or_like('tgl_lahir', $q);
    	$this->db->or_like('jk', $q);
    	$this->db->or_like('no_hp', $q);
    	$this->db->or_like('agama', $q);
    	$this->db->or_like('kewarganegaraan', $q);
    	$this->db->or_like('status', $q);
    	$this->db->or_like('pend_terkahir', $q);
    	$this->db->or_like('id_kecamatan', $q);
    	$this->db->or_like('id_kelurahan', $q);
    	$this->db->or_like('alamat', $q);
    	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id_seeker', $q);
    	$this->db->or_like('nama', $q);
    	$this->db->or_like('username', $q);
    	$this->db->or_like('email', $q);
    	$this->db->or_like('password', $q);
    	$this->db->or_like('tmp_lahir', $q);
    	$this->db->or_like('tgl_lahir', $q);
    	$this->db->or_like('jk', $q);
    	$this->db->or_like('no_hp', $q);
    	$this->db->or_like('agama', $q);
    	$this->db->or_like('kewarganegaraan', $q);
    	$this->db->or_like('status', $q);
    	$this->db->or_like('pend_terkahir', $q);
    	$this->db->or_like('id_kecamatan', $q);
    	$this->db->or_like('id_kelurahan', $q);
    	$this->db->or_like('alamat', $q);
    	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    public function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    public function update($id, $data){
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    public function delete($id){
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}