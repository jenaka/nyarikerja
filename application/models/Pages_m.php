<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pages_m extends CI_Model
{


    function __construct(){
        parent::__construct();
    }

    function getRows($params = array()){

        $this->db->select('job_slug, job_title, nama_perusahaan, logo_perusahaan, kota.name as nama_kota, nama_tipe, job_post.tgl as tgl_post');
        $this->db->from('job_post');
        $this->db->join('perusahaan', 'perusahaan.id_perusahaan=job_post.id_perusahaan');
        $this->db->join('kota', 'kota.id=job_post.id_kota');
        $this->db->join('job_tipe', 'job_tipe.id=job_post.id_job_tipe');

        if(!empty($params['search']['keywords'])){
            $this->db->like('title',$params['search']['keywords']);
        }
        //sort data by ascending or desceding order
        if(!empty($params['search']['sortBy'])){
            $this->db->order_by('title',$params['search']['sortBy']);
        }else{
            $this->db->order_by('id_job','desc');
        }
        //set start and limit
        if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->limit($params['limit'],$params['start']);
        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->limit($params['limit']);
        }
        //get records
        $query = $this->db->get();
        //return fetched data
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }

}
