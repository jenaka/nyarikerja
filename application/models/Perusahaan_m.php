<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Perusahaan_m extends CI_Model
{

    public $table = 'perusahaan';
    public $table2 = 'pegawai';
    public $id = 'id_perusahaan';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    public function id_perusahaan(){
        $q = $this->db->query("SELECT MAX(id_perusahaan) AS kd_max FROM perusahaan");
        $urut = "";
        if($q->num_rows()>0){
            foreach($q->result() as $k){
                $tmp = ((int)$k->kd_max)+1;
                $urut = sprintf("%01s", $tmp);
            }
        }else{
            $urut = "01";
        }
        return $urut;
    }

    public function check_email($email){
        $this->db->select('id, email, password, nama_lengkap, status, id_perusahaan');
        $this->db->from($this->table2);
        $this->db->join($this->table, 'id_perusahaan');
        $this->db->where('email',$email);
        return $this->db->get(); 
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    function get_profile_id($id){
        $this->db->where('id_perusahaan', $id);
         $this->db->join($this->table2, 'id_perusahaan');
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id_perusahaan', $q);
	$this->db->or_like('nama_perusahaan', $q);
	$this->db->or_like('email_perusahaan', $q);
	$this->db->or_like('telp_perusahaan', $q);
	$this->db->or_like('web_perusahaan', $q);
	$this->db->or_like('logo_perusahaan', $q);
	$this->db->or_like('id_jenis', $q);
	$this->db->or_like('deskripsi_perusahaan', $q);
	$this->db->or_like('log', $q);
	$this->db->or_like('lat', $q);
	$this->db->or_like('slug_perusahaan', $q);
	$this->db->or_like('status', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id_perusahaan', $q);
	$this->db->or_like('nama_perusahaan', $q);
	$this->db->or_like('email_perusahaan', $q);
	$this->db->or_like('telp_perusahaan', $q);
	$this->db->or_like('web_perusahaan', $q);
	$this->db->or_like('logo_perusahaan', $q);
	$this->db->or_like('id_jenis', $q);
	$this->db->or_like('deskripsi_perusahaan', $q);
	$this->db->or_like('log', $q);
	$this->db->or_like('lat', $q);
	$this->db->or_like('slug_perusahaan', $q);
	$this->db->or_like('status', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    public function simpan($pegawai, $perusahaan){
        $this->db->insert('perusahaan', $perusahaan);
        $this->db->insert('pegawai', $pegawai);
    }

    // update data
    public function update_pegawai($id, $pegawai){
        $this->db->where('id_perusahaan', $id);
        $this->db->update('pegawai', $pegawai);
    }

    public function update_perusahaan($id, $perusahaan){
        $this->db->where('id_perusahaan', $id);
        $this->db->update('perusahaan', $perusahaan);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}