<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Loker_m extends CI_Model
{

    public $table = 'job_post';
    public $id = 'id_job';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    function get_by_slug($job_slug){
        $this->db->select('job_slug, job_title, kota.name as nama_kota, nama_tipe, job_deskripsi');
        $this->db->select('nama_perusahaan, logo_perusahaan, deskripsi_perusahaan');
        $this->db->from($this->table);
        $this->db->join('perusahaan', 'perusahaan.id_perusahaan=job_post.id_perusahaan');
        $this->db->join('kota', 'kota.id=job_post.id_kota');
        $this->db->join('job_tipe', 'job_tipe.id=job_post.id_job_tipe');
        $this->db->where('job_slug', $job_slug);
        return $this->db->get()->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id_job', $q);
        $this->db->or_like('job_title', $q);
        $this->db->or_like('job_slug', $q);
        $this->db->or_like('id_kategori', $q);
        $this->db->or_like('id_sub_kategori', $q);
        $this->db->or_like('id_perusahaan', $q);
        $this->db->or_like('id_gaji', $q);
        $this->db->or_like('pengalaman', $q);
        $this->db->or_like('id_kualifikasi', $q);
        $this->db->or_like('id_kota', $q);
        $this->db->or_like('id_job_tipe', $q);
        $this->db->or_like('job_deskripsi', $q);
        $this->db->or_like('keahlian', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id_job', $q);
        $this->db->or_like('job_title', $q);
        $this->db->or_like('job_slug', $q);
        $this->db->or_like('id_kategori', $q);
        $this->db->or_like('id_sub_kategori', $q);
        $this->db->or_like('id_perusahaan', $q);
        $this->db->or_like('id_gaji', $q);
        $this->db->or_like('pengalaman', $q);
        $this->db->or_like('id_kualifikasi', $q);
        $this->db->or_like('id_kota', $q);
        $this->db->or_like('id_job_tipe', $q);
        $this->db->or_like('job_deskripsi', $q);
        $this->db->or_like('keahlian', $q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function simpan($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}