<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kategori_m extends CI_Model{

    var $table = 'job_kategori';


    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function get_all(){
        $this->db->order_by('id_kategori', 'DeSC');
        $this->db->from($this->table);
        return $this->db->get()->result();
    }

    public function simpan($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    
    public function get_kategori_id($id){
        $this->db->from($this->table);
        $this->db->where('id_kategori',$id);
        return $this->db->get()->row();
    }

    public function update($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function hapus($id){
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

// Sub Kategori
    public function get_sub_kategori_all(){
        $this->db->select('id_sub_kategori, job_sub_kategori.nama as nama_sub_kategori, job_sub_kategori.id_kategori as id_kategori');
        $this->db->order_by('job_sub_kategori.nama', 'asc');
        $this->db->join($this->table, 'job_kategori.id_kategori = job_sub_kategori.id_kategori');
        return $this->db->get('job_sub_kategori')->result();
    }

    public function get_sub_all($id){
        $this->db->order_by('id_sub_kategori', 'DeSC');
        $this->db->from('job_sub_kategori');
        $this->db->where('id_kategori', $id);
        return $this->db->get()->result();
    }

    public function sub_simpan($data){
        $this->db->insert('job_sub_kategori', $data);
        return $this->db->insert_id();
    }

    public function get_sub_kategori_id($id){
        $this->db->from($this->table);
        $this->db->where('id_kategori',$id);
        return $this->db->get()->row();
    }

    public function sub_hapus($id){
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

}
