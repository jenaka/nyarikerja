<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Industri_m extends CI_Model{

    var $table = 'job_indsutri';
    var $id = 'id_berita';
    var $column_order = array(null,'judul_berita','judul_kategori', 'tgl_berita');
    var $column_search = array('judul_berita','judul_kategori','tgl_berita');
    var $order = array('id_berita' => 'desc');


    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function get_all(){
        $this->db->order_by('id', 'DeSC');
        $this->db->from($this->table);
        return $this->db->get()->result();
    }

    public function simpan($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    
    public function get_kategori_id($id){
        $this->db->from($this->table);
        $this->db->where('id',$id);
        return $this->db->get()->row();
    }

    public function tambah_kategori($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function hapus($id){
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    public function hapus_berita($id){
        $this->db->where('id', $id);
        $this->db->delete($this->table2);
    }

}
