<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wilayah_m extends CI_Model{

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function get_provinsi(){
        $this->db->select('ifnull( (SELECT count(id) FROM (kota)WHERE(kota.province_id=provinsi.id)),0) AS jum_kota, provinsi.id as id, provinsi.name as nama');
        $this->db->order_by('provinsi.name', 'ASC');
        $this->db->from('provinsi');
        return $this->db->get()->result();
    }

    public function simpan($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    
    public function get_provinsi_id($id){
        $this->db->from('provinsi');
        $this->db->where('id',$id);
        return $this->db->get()->row();
    }

    public function tambah_kategori($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function hapus($id){
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    public function hapus_berita($id){
        $this->db->where('id', $id);
        $this->db->delete($this->table2);
    }

    public function get_kota_prov(){
        $this->db->select('kota.id as id, kota.name as nama_kota, kota.province_id as id_provinsi');
        $this->db->order_by('kota.name', 'asc');
        $this->db->join('provinsi', 'kota.province_id = provinsi.id');
        return $this->db->get('kota')->result();
    }

    public function get_kota($id){
        $this->db->select('ifnull( (SELECT count(id) FROM (districts)WHERE(districts.regency_id=kota.id)),0) AS jum_kec, kota.id as id, kota.name as nama');
        $this->db->order_by('kota.name', 'ASC');
        $this->db->from('kota');
        $this->db->where('province_id', $id);
        return $this->db->get()->result();
    }

    public function get_kota_id($id){
        $this->db->from('kota');
        $this->db->where('id',$id);
        return $this->db->get()->row();
    }

    public function get_kecamatan($id){
        $this->db->select('ifnull( (SELECT count(id) FROM (villages)WHERE(villages.district_id=districts.id)),0) AS jum_kel, districts.id as id, districts.name as nama');
        $this->db->order_by('districts.name', 'ASC');
        $this->db->from('districts');
        $this->db->where('regency_id', $id);
        return $this->db->get()->result();
    }

    public function get_kecamatan_id($id){
        $this->db->from('districts');
        $this->db->where('id',$id);
        return $this->db->get()->row();
    }

    public function get_kelurahan($id){
        $this->db->order_by('villages.name', 'ASC');
        $this->db->from('villages');
        $this->db->where('district_id', $id);
        return $this->db->get()->result();
    }

}
