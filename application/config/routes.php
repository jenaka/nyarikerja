<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['daftar/pencari-kerja'] = 'daftar/seeker';
$route['perusahaan'] = 'perusahaan/Data';
$route['admin'] = 'admin/login';

$route['lowongan-kerja'] = 'pages/loker';
$route['lowongan-kerja/(:any)'] = 'loker_detail/index/$1';
$route['default_controller'] = 'pages/index';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
