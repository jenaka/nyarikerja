var OpAuthSignUp = function() {
    var initValidationSignUp = function(){
        jQuery('.js-validation-signup').validate({
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'signup-username': {
                    required: true,
                    minlength: 3
                },
                'signup-email': {
                    required: true,
                    email: true
                },
                'signup-password': {
                    required: true,
                    minlength: 5
                },
                'signup-password-confirm': {
                    required: true,
                    equalTo: '#signup-password'
                }
            },
            messages: {
                'signup-username': {
                    required: 'Nama harus diisi',
                    minlength: 'Your username must consist of at least 3 characters'
                },
                'signup-email': 'Email harus diisi',
                'signup-password': {
                    required: 'Password harus diisi',
                    minlength: 'Password harus lebih dari 5 karakter'
                },
                'signup-password-confirm': {
                    required: 'Konfirmasi konfirmasi harus diisi',
                    minlength: 'Password harus lebih dari 5 karakter',
                    equalTo: 'Konfirmasi password tidak sama..'
                },
            }
        });
    };

    return {
        init: function () {
            initValidationSignUp();
        }
    };
}();

jQuery(function(){ OpAuthSignUp.init(); });